/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kmean.lib;

import com.kmean.controller.KMeanController;
import com.kmean.lib.model.Fiture;
import com.kmean.lib.model.CentroidCluster;
import com.kmean.lib.model.Distance;
import com.kmean.lib.model.KmeanData;
import com.kmean.model.AllData;
import com.kmean.model.KMeanSetting;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 *
 * @author Centry
 */
public class KMeanClustering {

    private ArrayList<KmeanData> kmeanDatas = new ArrayList<>();
    private KMeanSetting kMeanSetting;
    private KMeanController datasetController = new KMeanController();
    private double Fval = 0;
    private boolean clusterChange = true;

    public KMeanClustering(ArrayList<ArrayList<Integer>> dataset, KMeanSetting setting, List<Long> idList) {
        this.kMeanSetting = setting;
        for (int i = 0; i < dataset.size(); i++) {
//            KmeanData kmeanData = new 
            kmeanDatas.add(
                    new KmeanData(
                            idList.get(i),
                            dataset.get(i),
                            getRandomCluster(),
                            getFiture(dataset.get(i)),
                            getDistance())
            );

        }
    }

    //initial centroid
    private Fiture getFiture(ArrayList<Integer> dataset) {
        List<Integer> fitureList = new ArrayList<>();
        for (int i = 0; i < dataset.size(); i++) {
            fitureList.add(dataset.get(i));
        }
        return new Fiture(fitureList);
    }

//    init distance
    private Distance getDistance() {
        List<Double> distance = new ArrayList<>();
        for (int i = 0; i < kMeanSetting.getCluster(); i++) {
            distance.add(0.0);
        }
        return new Distance(distance);
    }

    private int getRandomCluster() {
        return new Random().nextInt(kMeanSetting.getCluster());
    }

    public ArrayList<KmeanData> getKmeanDatas() {
        return kmeanDatas;
    }

    public Map<Long,Integer> calculateKMean() {
        Map<Long,Integer> dataMap = new HashMap<>();
        do {
            CentroidCluster centroidCluster = calculateCentroidCluster();
            List<List<Double>> distanceFitureToCentroids = distanceFitureToCentroid(centroidCluster);
            reClustering(distanceFitureToCentroids);
            setFval(distanceFitureToCentroids);

        } while (getFval() > kMeanSetting.getTreshold() && isClusterChange() == true);

        if (getFval() < kMeanSetting.getTreshold()) {
            System.out.println("====================  FINISH BY TRESHOLD ===================");
        } else {
            System.out.println("====================  FINISH BY NO CHANGE DATA ============================");
        }
        
        // generating data map
        for (int i = 0; i < kmeanDatas.size(); i++) {
            dataMap.put(kmeanDatas.get(i).getId(), kmeanDatas.get(i).getCluster());
        }
        return dataMap;
    }

    private void setFval(List<List<Double>> distance) {
        double total = 0;
        for (int i = 0; i < distance.size(); i++) {
            for (int j = 0; j < distance.size(); j++) {
                total += distance.get(i).get(j);
            }
        }
        Fval = total - getFval();
    }

    private double getFval() {
        return Fval;
    }

    public boolean isClusterChange() {
        return clusterChange;
    }

    public void setClusterChange(boolean clusterChange) {
        this.clusterChange = clusterChange;
    }

    public CentroidCluster calculateCentroidCluster() {

        List<Integer> sumCluster = new ArrayList<>();
        List<List<Integer>> sumFiture = new ArrayList<>();
        List<List<Double>> centroid = new ArrayList<>();
        //calculated Cluster;
        int sum = 0;
        for (int i = 0; i < kMeanSetting.getCluster(); i++) {
            for (int j = 0; j < kmeanDatas.size(); j++) {
                if (kmeanDatas.get(j).getCluster() == i) {
                    sum++;
                }
            }
            sumCluster.add(sum);
            sum = 0;
        }
        //callculate fiture;
        for (int i = 0; i < kMeanSetting.getCluster(); i++) {
            List<Integer> countFiture = new ArrayList<>();
            for (int j = 0; j < kmeanDatas.get(0).getData().size(); j++) {
                for (int k = 0; k < kmeanDatas.size(); k++) {
                    if (kmeanDatas.get(k).getCluster() == i) {
                        sum += kmeanDatas.get(k).getData().get(j);
                    }
                }
                countFiture.add(sum);
                sum = 0;
            }
            sumFiture.add(countFiture);
        }

        System.out.println("LIST sumCluster > " + sumCluster.toString());
        for (int i = 0; i < sumFiture.size(); i++) {
            System.out.println("LIST sumFuture [" + i + "] ->" + sumFiture.get(i).toString());
        }
        System.out.println("###################################################");
        //calculate centroid
        for (int i = 0; i < sumFiture.size(); i++) {
            List<Double> sumCentroid = new ArrayList<>();
            for (int j = 0; j < sumFiture.get(i).size(); j++) {
                sumCentroid.add(calculateCentroid(sumFiture.get(i).get(j), sumCluster.get(i)));
            }
            centroid.add(sumCentroid);
            System.out.println("Centroid value [" + i + "] > " + sumCentroid.toString());

        }
//        List<List<Double>> distance = distanceFitureToCentroid(new CentroidCluster(sumCluster, sumFiture, centroid));
//        reClustering(distance);

        return new CentroidCluster(sumCluster, sumFiture, centroid);
    }

    private double calculateCentroid(int totalClusterFitureX, int totalCluster) {
        if (totalCluster == 0) {
            return 0;
        }
        double centroid = (double) totalClusterFitureX / (double) totalCluster;
        return centroid;
    }

    private List<List<Double>> distanceFitureToCentroid(CentroidCluster centroidCluster) {
        double sum = 0;
        List<List<Double>> distance = new ArrayList<>();
        for (int i = 0; i < centroidCluster.getCentroid().size(); i++) {
            System.out.println("#############################################");
            List<Double> count = new ArrayList<>();
            for (int j = 0; j < kmeanDatas.size(); j++) {
                for (int k = 0; k < kmeanDatas.get(j).getData().size(); k++) {
                    sum += (Math.pow(
                            kmeanDatas.get(j).getData().get(k) - centroidCluster.getCentroid().get(i).get(k), 2));
                    System.out.print("/ " + (Math.pow(
                            kmeanDatas.get(j).getData().get(k) - centroidCluster.getCentroid().get(i).get(k), 2)));

                }

                System.out.println("");

                sum = (double) Math.sqrt(sum);
                System.out.println("SQRT = " + sum);

                count.add(sum);
                sum = 0;
            }
            distance.add(count);
        }
        System.out.println("##############################");
        for (int i = 0; i < distance.size(); i++) {
            System.out.println("Distance Cluster [" + i + "] ->" + distance.get(i).toString());
        }
        return distance;
    }

    private void reClustering(List<List<Double>> distance) {
        double val = 0;
        int cluster = 0;
        boolean changeStatus = false;
        for (int i = 0; i < kmeanDatas.size(); i++) {

            for (int j = 0; j < distance.size(); j++) {
                if (j == 0) {
                    val = distance.get(j).get(i);
                }
                if (val > distance.get(j).get(i)) {
                    val = distance.get(j).get(i);
                    cluster = j;
                }
                System.out.println("VAL 1 = " + val);
                System.out.println("VAl 2 = " + distance.get(j).get(i));
                System.out.println("-----------------------");

            }
            if (kmeanDatas.get(i).getCluster() != cluster) {
                changeStatus = true;
            }

            System.out.println("From -> " + kmeanDatas.get(i).getCluster() + "  to -> " + cluster);
            System.out.println("==========================================");
            kmeanDatas.get(i).setCluster(cluster);

        }
        setClusterChange(changeStatus);
    }
}
