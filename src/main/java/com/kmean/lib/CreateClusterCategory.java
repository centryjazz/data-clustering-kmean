/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kmean.lib;

import com.kmean.model.ClusteringPerangkat;
import com.kmean.model.KMeanCluster;
import com.kmean.model.Perangkat;
import com.kmean.repository.KMeanClusterRepository;
import com.kmean.repository.KlasifikasiPerangkatRepository;
import com.kmean.repository.PerangkatRepository;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Centry
 */
public class CreateClusterCategory {

    @Autowired
    private KMeanClusterRepository kMeanClusterRepository;

    @Autowired
    private PerangkatRepository perangkatRepository;

    @Autowired
    private KlasifikasiPerangkatRepository klasifikasiPerangkatRepository;

    public Map<String, Boolean> createCategory() {
        Map<String, Boolean> status = new HashMap<>();
        try {

            List<KMeanCluster> data = kMeanClusterRepository.getDataByIdASC();
            List<Integer> cluster = kMeanClusterRepository.getCluster();
            List<Perangkat> perangkat = perangkatRepository.getAllDataOrderByID();
            List<Integer> newClusterCAtegory = new ArrayList<>();
            List<ClusteringPerangkat> clusteringPerangkats = new ArrayList<>();
            for (int i = 0; i < cluster.size(); i++) {

                int idx = 0;
                for (int j = 0; j < data.size(); j++) {
                    if (data.get(j).getRecomended_cluster() == cluster.get(i)) {
                        if (perangkat.get(idx).getTahun_pembuatan() < perangkat.get(j).getTahun_pembuatan()
                                && Integer.parseInt(perangkat.get(idx).getRam()) < Integer.parseInt(perangkat.get(j).getRam())
                                && Integer.parseInt(perangkat.get(idx).getHdd()) < Integer.parseInt(perangkat.get(j).getHdd())) {
                            idx = j;
                        }
                    }
                }
                newClusterCAtegory.add(idx);
            }

            for (int i = 0; i < newClusterCAtegory.size(); i++) {

                Perangkat p = perangkatRepository.getOneData(
                        perangkat.get(
                                newClusterCAtegory.get(i)
                        ).getId()
                );
                ClusteringPerangkat cp
                        = new ClusteringPerangkat(
                                i,
                                newClusterCAtegory.get(i),
                                p.getJenis(),
                                p.getOs(),
                                p.getProsesor(),
                                Integer.parseInt(p.getRam()),
                                Integer.parseInt(p.getHdd()),
                                p.getMonitor(),
                                "-",
                                p.getAntarmuka(),
                                "-",
                                "-",
                                p.getGaransi());

                clusteringPerangkats.add(cp);

            }

            klasifikasiPerangkatRepository.deleteAll();
            klasifikasiPerangkatRepository.saveAll(clusteringPerangkats);
            status.put("Status", Boolean.FALSE);
        } catch (Exception e) {
            e.printStackTrace();
            status.put("Status", Boolean.FALSE);
        }
        return status;
    }
}
