/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kmean.lib.model;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 *
 * @author Centry
 */
@Data
@AllArgsConstructor
public class CentroidCluster {
    List<Integer> cluster;
    List<List<Integer>> calculatedFiture;
    List<List<Double>> centroid;
}
