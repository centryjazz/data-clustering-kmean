/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kmean.lib.model;

import com.kmean.lib.model.Fiture;
import com.kmean.lib.model.Distance;
import com.kmean.model.AllData;
import java.util.ArrayList;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class KmeanData {

    private long id;
    private ArrayList<Integer> data;
    private int cluster;
    private Fiture centroid;
    private Distance distance;
}
