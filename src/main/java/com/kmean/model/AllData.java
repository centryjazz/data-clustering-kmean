/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kmean.model;

//import javax.persistence.Column;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Centry
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AllData {

    private long id;
    private String jenis;
    private String merk;
    private String type;
    private String prosesor;
    private String os;
    private int ram;
    private int hdd;
    private String vga;
    private String monitor;
    private String antarmuka;
    private String mac_address;
    private int tahun_pembuatan;
    private String garansi;
    private String status_perangkat;
    private int tahun_awal_operasi;
    private String owner;
    private String pengelola;
    private String nip;
    private String nama;
    private String email;
    private String deskripsi;
    private String nama_bidang;
    private String status_cluster;
    private long cluster;
    private long cluster_recomended;
    
    
}
