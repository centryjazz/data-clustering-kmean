/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kmean.model;

import com.kmean.repository.PerangkatRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Centry
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Dashboard {
    
   private int dataUpgrade;
   private int dataDowngrade;
   private int dataStable;
   private int totalDataChange;
   
   private int totalCluster;
   private int beforecluster;
   private int curentCluster;
   
   private int totalData;
   
   
}
