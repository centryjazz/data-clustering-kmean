/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kmean.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Centry
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "perangkat")
public class Perangkat {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String jenis;
    private String merk;
    private String type;
    private String prosesor;
    private String os;
    private String ram;
    private String hdd;
    private String vga;
    private String monitor;
    private String antarmuka;
    private String mac_address;
    private int tahun_pembuatan;
    private String garansi;
    private String status_perangkat;
    private int tahun_awal_operasi;
    @Column(name = "id_owner",nullable = false)
    private long idOwner;
    @Column(name = "id_pengguna", nullable = false)
    private long idPengguna;
    @Column(name = "id_cluster", nullable = true)
    private long idCluster;
}
