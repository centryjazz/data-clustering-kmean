/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kmean.controller;

import com.kmean.exception.ResourceNotFoundException;
import com.kmean.model.KMeanCluster;
import com.kmean.model.Pemilik;
import com.kmean.model.Pengguna;
import com.kmean.model.Perangkat;
import com.kmean.repository.KMeanClusterRepository;
import com.kmean.repository.PemilikRepository;
import com.kmean.repository.PenggunaRepository;
import com.kmean.repository.PerangkatRepository;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Centry
 */
@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/perangkat")
public class PerangkatController {
    
    @Autowired
    private PerangkatRepository perangkatRepository;
    @Autowired
    private PenggunaRepository penggunaRepository;
    @Autowired
    private PemilikRepository pemilikRepository;
    @Autowired
    private KMeanClusterRepository kMeanClusterRepository;
    
    @GetMapping("/all")
    public List<Perangkat> getAll() {
        return perangkatRepository.getAllDataOrderByID();
    }
    
    @GetMapping("/pengguna/{id}")
    public List<Perangkat> getAllPerangkatsByPengunaId(@PathVariable(value = "id") Long idPengguna) {
        return perangkatRepository.findByidPengguna(idPengguna);
    }
    
    @GetMapping("/owner/{id}")
    public List<Perangkat> getAllPerangkatsByOwnerId(@PathVariable(value = "id") Long idOwner) {
        return perangkatRepository.findByidOwner(idOwner);
    }
    
    @GetMapping("/{id}")
    public ResponseEntity<Perangkat> getPerangkatById(@PathVariable(value = "id") Long perangkatId)
            throws ResourceNotFoundException {
        Perangkat perangkat = perangkatRepository.findById(perangkatId)
                .orElseThrow(() -> new ResourceNotFoundException("Perangkat not found for this id :: " + perangkatId));
        return ResponseEntity.ok().body(perangkat);
    }
    @GetMapping("/single/{id}")
    public Perangkat getsinglePerangkatById(@PathVariable(value = "id") Long perangkatId){
        return perangkatRepository.getOneData(perangkatId);
    }
    
    @PostMapping("/add")
    public Perangkat createPerangkat(@Valid @RequestBody Perangkat perangkat){
//        long id = getID();
//        kMeanClusterRepository.save(new KMeanCluster(id, -1, -1, "-"));
//        perangkat.setIdCluster(id);
        return perangkatRepository.save(perangkat);
    }
    
    @PutMapping("/edit/{id}")
    public ResponseEntity<Perangkat> updatePerangkat(@PathVariable(value = "id") Long perangkatId,
            @Valid @RequestBody Perangkat perangkatData) throws ResourceNotFoundException {
        
        Pengguna pengguna = penggunaRepository.findById(perangkatData.getIdPengguna())
                .orElseThrow(() -> new ResourceNotFoundException("Pengguna not found for this id :: " + perangkatData.getIdPengguna()));
        
        Pemilik pemilik = pemilikRepository.findById(perangkatData.getIdOwner())
                .orElseThrow(() -> new ResourceNotFoundException("Pemilik not found for this id :: " + perangkatData.getIdOwner()));
        
        Perangkat perangkat = perangkatRepository.findById(perangkatId)
                .orElseThrow(() -> new ResourceNotFoundException("Perangkat not found for this id :: " + perangkatId));
        
        perangkat.setJenis(perangkatData.getJenis());
        perangkat.setMerk(perangkatData.getMerk());
        perangkat.setType(perangkatData.getType());
        perangkat.setProsesor(perangkatData.getProsesor());
        perangkat.setOs(perangkatData.getOs());
        perangkat.setRam(perangkatData.getRam());
        perangkat.setHdd(perangkatData.getHdd());
        perangkat.setVga(perangkatData.getVga());
        perangkat.setMonitor(perangkatData.getMonitor());
        perangkat.setAntarmuka(perangkatData.getAntarmuka());
        perangkat.setMac_address(perangkatData.getMac_address());
        perangkat.setTahun_pembuatan(perangkatData.getTahun_pembuatan());
        perangkat.setGaransi(perangkatData.getGaransi());
        perangkat.setTahun_awal_operasi(perangkatData.getTahun_awal_operasi());
        perangkat.setIdOwner(perangkatData.getIdOwner());
        perangkat.setIdPengguna(perangkatData.getIdPengguna());
        perangkat.setIdCluster(perangkatData.getIdCluster());
        
        final Perangkat updatedPerangkat = perangkatRepository.save(perangkat);
        return ResponseEntity.ok(updatedPerangkat);
    }
    
    @DeleteMapping("/remove/{id}")
    public Map<String, Boolean> deletePerangkat(@PathVariable(value = "id") Long perangkatId)
            throws ResourceNotFoundException {
        Perangkat perangkat = perangkatRepository.findById(perangkatId)
                .orElseThrow(() -> new ResourceNotFoundException("Perangkat not found for this id :: " + perangkatId));
        
        perangkatRepository.delete(perangkat);
        Map<String, Boolean> response = new HashMap<>();
        response.put("Status", Boolean.TRUE);
        return response;
    }
    
    public long getID() {
        long id = 0;
        Date date = (Date) Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("yyyymmddhhmmss");
        id = Long.parseLong(dateFormat.format(date));
        return id;
    }
}
