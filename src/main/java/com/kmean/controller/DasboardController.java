/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kmean.controller;

import com.kmean.model.Dashboard;
import com.kmean.model.KMeanCluster;
import com.kmean.repository.KMeanClusterRepository;
import com.kmean.repository.KlasifikasiPerangkatRepository;
import com.kmean.repository.PerangkatRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/dashboard")
public class DasboardController {

    @Autowired
    private PerangkatRepository perangkatRepository;
    @Autowired
    private KlasifikasiPerangkatRepository klasifikasiPerangkatRepository;
    @Autowired
    private KMeanClusterRepository kMeanClusterRepository;

    @GetMapping("/")
    public Dashboard getSumary() {

        return new Dashboard(
                getTotalDataUpgrade(),
                getTotalDataDownGrade(),
                getTotalDataStable(),
                getTotalDataChange(),
                kMeanClusterRepository.getTotalCluster(),
                kMeanClusterRepository.getTotalUsedClusterBefore(),
                klasifikasiPerangkatRepository.findAll().size(),
                perangkatRepository.getIdList().size());
    }

    private int getTotalDataChange() {

        int count = 0;
        List<KMeanCluster> datas = kMeanClusterRepository.findAll();
        for (int i = 0; i < datas.size(); i++) {
            if (datas.get(i).getCurent_cluster() != datas.get(i).getRecomended_cluster()) {
                count++;
            }
        }

        return count;
    }

    private int getTotalDataUpgrade() {
        int count = 0;
        List<KMeanCluster> datas = kMeanClusterRepository.findAll();
        for (int i = 0; i < datas.size(); i++) {
            if (datas.get(i).getCurent_cluster() < datas.get(i).getRecomended_cluster()) {
                count++;
            }
        }

        return count;
    }

    private int getTotalDataDownGrade() {
        int count = 0;
        List<KMeanCluster> datas = kMeanClusterRepository.findAll();
        for (int i = 0; i < datas.size(); i++) {
            if (datas.get(i).getCurent_cluster() > datas.get(i).getRecomended_cluster()) {
                count++;
            }
        }

        return count;
    }

    private int getTotalDataStable() {
        int count = 0;
        List<KMeanCluster> datas = kMeanClusterRepository.findAll();
        for (int i = 0; i < datas.size(); i++) {
            if (datas.get(i).getCurent_cluster()  == datas.get(i).getRecomended_cluster()) {
                count++;
            }
        }

        return count;
    }
}
