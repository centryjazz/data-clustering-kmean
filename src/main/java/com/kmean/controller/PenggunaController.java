/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kmean.controller;

import com.kmean.exception.ResourceNotFoundException;
import com.kmean.model.Bidang;
import com.kmean.model.Pengguna;
import com.kmean.repository.BidangRepository;
import com.kmean.repository.PenggunaRepository;
import com.kmean.repository.PenggunaRepository;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Centry
 */
@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/pengguna")
public class PenggunaController {
    @Autowired
    private PenggunaRepository penggunaRepository;
    @Autowired
    private BidangRepository bidangRepository;
    
    @GetMapping("/all")
    public List<Pengguna> getAll() {
        return penggunaRepository.getAllDataOrderByID();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Pengguna> getPenggunaById(@PathVariable(value = "id") Long penggunaId)
            throws ResourceNotFoundException {
        Pengguna pengguna = penggunaRepository.findById(penggunaId)
                .orElseThrow(() -> new ResourceNotFoundException("Pengguna not found for this id :: " + penggunaId));
        return ResponseEntity.ok().body(pengguna);
    }

    @PostMapping("/add")
    public Pengguna createPengguna(@Valid @RequestBody Pengguna pengguna) throws ResourceNotFoundException {
        Bidang bidang = bidangRepository.findById(pengguna.getBidang())
                .orElseThrow(() -> new ResourceNotFoundException("Bidang not found for this id :: " + pengguna.getBidang()));
        
        return penggunaRepository.save(pengguna);
    }

    @PutMapping("/edit/{id}")
    public ResponseEntity<Pengguna> updatePengguna(@PathVariable(value = "id") Long penggunaId,
            @Valid @RequestBody Pengguna penggunaData) throws ResourceNotFoundException {
        
        Bidang bidang = bidangRepository.findById(penggunaData.getBidang())
                .orElseThrow(() -> new ResourceNotFoundException("Bidang not found for this id :: " + penggunaData.getBidang()));
        
        Pengguna pengguna = penggunaRepository.findById(penggunaId)
                .orElseThrow(() -> new ResourceNotFoundException("Pengguna not found for this id :: " + penggunaId));
        
        
        pengguna.setNip(penggunaData.getNip());
        pengguna.setNama(penggunaData.getNama());
        pengguna.setEmail(penggunaData.getEmail());
        pengguna.setDeskripsi(penggunaData.getDeskripsi());
        pengguna.setBidang(penggunaData.getBidang());
        
        final Pengguna updatedPengguna = penggunaRepository.save(pengguna);
        return ResponseEntity.ok(updatedPengguna);
    }

    @DeleteMapping("/remove/{id}")
    public Map<String, Boolean> deletePengguna(@PathVariable(value = "id") Long penggunaId)
            throws ResourceNotFoundException {
        Pengguna pengguna = penggunaRepository.findById(penggunaId)
                .orElseThrow(() -> new ResourceNotFoundException("Pengguna not found for this id :: " + penggunaId));

        penggunaRepository.delete(pengguna);
        Map<String, Boolean> response = new HashMap<>();
        response.put("Status", Boolean.TRUE);
        return response;
    }
}
