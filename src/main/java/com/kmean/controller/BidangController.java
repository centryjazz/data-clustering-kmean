/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kmean.controller;

import com.kmean.exception.ResourceNotFoundException;
import com.kmean.model.Bidang;
import com.kmean.repository.BidangRepository;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Centry
 */
@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/bidang")
public class BidangController {
    
    @Autowired
    private BidangRepository bidangRepository;
    
    @GetMapping("/all")
    public List<Bidang> getAll() {
        return bidangRepository.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Bidang> getBidangById(@PathVariable(value = "id") Long bidangId)
            throws ResourceNotFoundException {
        Bidang bidang = bidangRepository.findById(bidangId)
                .orElseThrow(() -> new ResourceNotFoundException("Bidang not found for this id :: " + bidangId));
        return ResponseEntity.ok().body(bidang);
    }

    @PostMapping("/add")
    public Bidang createBidang(@Valid @RequestBody Bidang perangkat) {
        return bidangRepository.save(perangkat);
    }

    @PutMapping("/edit/{id}")
    public ResponseEntity<Bidang> updateBidang(@PathVariable(value = "id") Long bidangId,
            @Valid @RequestBody Bidang bidangData) throws ResourceNotFoundException {
        Bidang bidang = bidangRepository.findById(bidangId)
                .orElseThrow(() -> new ResourceNotFoundException("Bidang not found for this id :: " + bidangId));
        bidang.setNama_bidang(bidangData.getNama_bidang());
        final Bidang updatedBidang = bidangRepository.save(bidang);
        return ResponseEntity.ok(updatedBidang);
    }

    @DeleteMapping("/remove/{id}")
    public Map<String, Boolean> deleteBidang(@PathVariable(value = "id") Long bidangId)
            throws ResourceNotFoundException {
        Bidang bidang = bidangRepository.findById(bidangId)
                .orElseThrow(() -> new ResourceNotFoundException("Bidang not found for this id :: " + bidangId));

        bidangRepository.delete(bidang);
        Map<String, Boolean> response = new HashMap<>();
        response.put("Status", Boolean.TRUE);
        return response;
    }
}
