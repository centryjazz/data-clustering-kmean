/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kmean.controller;

import com.kmean.model.User;
import com.kmean.repository.UserRepository;
import java.util.HashMap;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Centry
 */
@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/service")
public class LoginController {

    @Autowired
    private UserRepository userRepository;

    @PostMapping("/login")
    public Map<String, Boolean> loginUser(@Valid @RequestBody User user) {
        User us = userRepository.getUserAcount(user.getUsername());
        Map<String, Boolean> status = new HashMap<>();
        status.put("Status", Boolean.FALSE);
//        System.out.println("USERNAME PASSWORD REQ = " + user.getUsername() + " // " + user.getPassword());
//        System.out.println("USERNAME PASSWORD JPA = " + us.getUsername() + " // " + us.getPassword());

        if (us.getUsername().equals(user.getUsername()) && us.getPassword().equals(user.getPassword())) {
            status.put("Status", Boolean.TRUE);
        }

        return status;
    }
}
