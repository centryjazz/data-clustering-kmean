/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kmean.controller;

import com.kmean.exception.ResourceNotFoundException;
import com.kmean.model.Pemilik;
//import com.kmean.repository.PemilikRepository;
import com.kmean.repository.PemilikRepository;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Centry
 */
@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/pemilik")
public class PemilikController {
    @Autowired
    private PemilikRepository pemilikRepository;
    
    @GetMapping("/all")
    public List<Pemilik> getAll() {
        return pemilikRepository.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Pemilik> getPemilikById(@PathVariable(value = "id") Long pemilikId)
            throws ResourceNotFoundException {
        Pemilik pemilik = pemilikRepository.findById(pemilikId)
                .orElseThrow(() -> new ResourceNotFoundException("Pemilik not found for this id :: " + pemilikId));
        return ResponseEntity.ok().body(pemilik);
    }
    @GetMapping("/getone/{id}")
    public Pemilik getOnePemilik(@PathVariable(value = "id") Long id){
        return pemilikRepository.getOneData(id);
    }
    @PostMapping("/add")
    public Pemilik createPemilik(@Valid @RequestBody Pemilik pemilik) {
        return pemilikRepository.save(pemilik);
    }

    @PutMapping("/edit/{id}")
    public ResponseEntity<Pemilik> updatePemilik(@PathVariable(value = "id") Long pemilikId,
            @Valid @RequestBody Pemilik pemilikData) throws ResourceNotFoundException {
        Pemilik pemilik = pemilikRepository.findById(pemilikId)
                .orElseThrow(() -> new ResourceNotFoundException("Pemilik not found for this id :: " + pemilikId));
        
        pemilik.setNama(pemilikData.getNama());
        pemilik.setPengelola(pemilikData.getPengelola());
        
        final Pemilik updatedPemilik = pemilikRepository.save(pemilik);
        return ResponseEntity.ok(updatedPemilik);
    }

    @DeleteMapping("/remove/{id}")
    public Map<String, Boolean> deletePemilik(@PathVariable(value = "id") Long pemilikId)
            throws ResourceNotFoundException {
        Pemilik pemilik = pemilikRepository.findById(pemilikId)
                .orElseThrow(() -> new ResourceNotFoundException("Pemilik not found for this id :: " + pemilikId));

        pemilikRepository.delete(pemilik);
        Map<String, Boolean> response = new HashMap<>();
        response.put("Status", Boolean.TRUE);
        return response;
    }
}
