/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kmean.controller;

//import com.kmean.lib.DataSet;
import com.kmean.exception.ResourceNotFoundException;
import com.kmean.lib.CreateClusterCategory;
import com.kmean.lib.KMeanClustering;
import com.kmean.lib.model.CentroidCluster;
//import com.kmean.lib.model.DataTracking;
//import com.kmean.lib.model.KmeanData;
import com.kmean.model.AllData;
import com.kmean.model.Bidang;
import com.kmean.model.ClusteringPerangkat;
import com.kmean.model.KMeanCluster;
import com.kmean.model.KMeanSetting;
import com.kmean.model.Pemilik;
import com.kmean.model.Pengguna;
import com.kmean.model.Perangkat;
import com.kmean.repository.BidangRepository;
import com.kmean.repository.KMeanClusterRepository;
import com.kmean.repository.KMeanSettingRepository;
import com.kmean.repository.KlasifikasiPerangkatRepository;
import com.kmean.repository.PemilikRepository;
import com.kmean.repository.PenggunaRepository;
import com.kmean.repository.PerangkatRepository;
//import com.kmean.repository.VariableRepository;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
//import java.util.stream.Collectors;
//import javax.crypto.Mac;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Centry
 */
@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/kmean")
public class KMeanController {

    @Autowired
    private BidangRepository bidangRepository;

    @Autowired
    private PemilikRepository pemilikRepository;

    @Autowired
    private PenggunaRepository penggunaRepository;

    @Autowired
    private PerangkatRepository perangkatRepository;

    @Autowired
    private KMeanSettingRepository kMeanSettingRepository;

    @Autowired
    private KMeanClusterRepository kMeanClusterRepository;

    @Autowired
    private KlasifikasiPerangkatRepository klasifikasiPerangkatRepository;

    @GetMapping("/dataset/raw")
    public ArrayList<AllData> getAllRawDataset() {
        return getRawDataset();

    }
    
    @GetMapping("/dataset/raw/{id}")
    public AllData getSiggleRawData(@PathVariable(value = "id") Long id){
        ArrayList<AllData> datas = getRawDataset();
        AllData data = null;
        for (int i = 0; i < datas.size(); i++) {
            if(datas.get(i).getId() == id){
                data = datas.get(i);
            }
        }
        return data;
    }

    @GetMapping("/dataset/")
    public ArrayList<AllData> getObjectDataset() {
        return converToObject(getClearDataSet(), perangkatRepository.getIdList(), getVariable());
    }

    @GetMapping("/klasifikasi/perangkat")
    public List<ClusteringPerangkat> getClusteringPerangkat() {
        return klasifikasiPerangkatRepository.findAll();
    }

    @GetMapping("/klasifikasi/perangkat/update")
    public Map<String, Boolean> updateKlasifikasiPerangkat() {
        return createCategory();
    }

    @GetMapping("/setting")
    public KMeanSetting getSetting() {
        return kMeanSettingRepository.getDataSetting();
    }
    
    @GetMapping("/klasifikasi/{id}")
    public ClusteringPerangkat getSiggleClusterPerangkat(@PathVariable(value = "id") Long id){
        return klasifikasiPerangkatRepository.getSigleData(id);
    }

    @PostMapping("/setting/edit")
    public KMeanSetting createSetting(@Valid @RequestBody KMeanSetting setting) {
        KMeanSetting kmeanSetting = kMeanSettingRepository.getDataSetting();

        setting.setId(kmeanSetting.getId());
        return kMeanSettingRepository.save(setting);
    }

    @GetMapping("/all")
    public List<AllData> getClusterData() {
        return getRawDataset();
    }

    @GetMapping("/data/cluster")
    public List<KMeanCluster> kmeanCluster() {
        return kMeanClusterRepository.getDataByIdASC();
    }
    @PostMapping("data/cluster/add")
    public KMeanCluster addCluster(@RequestBody KMeanCluster kMeanCluster){
        return kMeanClusterRepository.save(kMeanCluster);
    }
//    @PostMapping("data/cluster/edit/{id}")
//    public KMeanCluster editCluster(@RequestBody KMeanCluster kMeanCluster){
//        return kMeanClusterRepository.save(kMeanCluster);
//    }
    @DeleteMapping("data/cluster/delete/{id}")
    public Map<String, Boolean> deleteDataCluster(@PathVariable(value = "id") Long idCluster) throws ResourceNotFoundException{
         KMeanCluster kMeanCluster = kMeanClusterRepository.findById(idCluster)
                .orElseThrow(() -> new ResourceNotFoundException("cluster not found for this id :: " + idCluster));

        kMeanClusterRepository.delete(kMeanCluster);
        Map<String, Boolean> response = new HashMap<>();
        response.put("Status", Boolean.TRUE);
        return response;
    }
    @GetMapping("/update")
    public Map<String, Boolean> updateCluster() {
        Map<Long, Integer> dataCluster = getKmean();
        Map<String, Boolean> status = new HashMap<>();
        status.put("Status", Boolean.FALSE);

        try {
            if (dataCluster.isEmpty()) {
                return status;
            }
            System.out.println("DATA CLuster size : "+dataCluster.size());
            
            List<KMeanCluster> kMeanClusterings = kMeanClusterRepository.getDataByIdASC();
            if (kMeanClusterings.isEmpty()) {

                return status;
            }
            System.out.println("DATA KMEan Clustering size : "+kMeanClusterings.size());
            for (int i = 0; i < kMeanClusterings.size(); i++) {
                kMeanClusterings.get(i).setCurent_cluster(kMeanClusterings.get(i).getRecomended_cluster());
                kMeanClusterings.get(i).setRecomended_cluster(dataCluster.get((long)kMeanClusterings.get(i).getId()));
                if (dataCluster.get((long) kMeanClusterings.get(i).getId()) == kMeanClusterings.get(i).getCurent_cluster()) {
                    kMeanClusterings.get(i).setStatus("STANDARD");
                } else if (dataCluster.get((long) kMeanClusterings.get(i).getId()) < kMeanClusterings.get(i).getCurent_cluster()) {
                    kMeanClusterings.get(i).setStatus("DOWN GRADE");
                } else {
                    kMeanClusterings.get(i).setStatus("UPGRADE");
                }
//                kMeanClusterings.get(i).setCurent_cluster(dataCluster.get((long) (i + 1)));
            }
            kMeanClusterRepository.deleteAll();
            kMeanClusterRepository.saveAll(kMeanClusterings);
            status.put("Status", Boolean.TRUE);
            createCategory();
        } catch (Exception e) {
            e.printStackTrace();

        }

        return status;
    }

    public Map<Long, Integer> getKmean() {
        KMeanClustering kMeanClustering = null;
        try {
            kMeanClustering = new KMeanClustering(getClearDataSet(), kMeanSettingRepository.getDataSetting(), perangkatRepository.getIdList());

        } catch (Exception e) {
            e.printStackTrace();
        }
//        return new DataTracking(kMeanClustering.getKmeanDatas(), kMeanClustering.calculateCentroidCluster());
        return kMeanClustering.calculateKMean();
    }

    public ArrayList<ArrayList<Integer>> getClearDataSet() {
        System.out.println("#~> " + getVariable().toString());
        return getDataSet(getVariable(), getRawDataset());
    }

    private ArrayList<AllData> converToObject(ArrayList<ArrayList<Integer>> clearDataset, List<Long> idList, ArrayList<Integer> variables) {
        ArrayList<AllData> dataset = new ArrayList<>();
        for (int i = 0; i < clearDataset.size(); i++) {
            AllData allData = new AllData();
            allData.setId(idList.get(i));
            for (int j = 0; j < variables.size(); j++) {
                switch (variables.get(j)) {
                    case 1:
                        allData.setJenis(Integer.toString(clearDataset.get(i).get(j)));
                        break;
                    case 2:
                        allData.setMerk(Integer.toString(clearDataset.get(i).get(j)));
                        break;
                    case 3:
                        allData.setType(Integer.toString(clearDataset.get(i).get(j)));
                        break;
                    case 4:
                        allData.setProsesor(Integer.toString(clearDataset.get(i).get(j)));
                        break;
                    case 5:
                        allData.setOs(Integer.toString(clearDataset.get(i).get(j)));
                        break;
                    case 6:
                        allData.setRam(clearDataset.get(i).get(j));
                        break;
                    case 7:
                        allData.setHdd(clearDataset.get(i).get(j));
                        break;
                    case 8:
                        allData.setVga(Integer.toString(clearDataset.get(i).get(j)));
                        break;
                    case 9:
                        allData.setMonitor(Integer.toString(clearDataset.get(i).get(j)));
                        break;
                    case 10:
                        allData.setAntarmuka(Integer.toString(clearDataset.get(i).get(j)));
                        break;
                    case 11:
                        allData.setMac_address(Integer.toString(clearDataset.get(i).get(j)));
                        break;
                    case 12:
                        allData.setTahun_pembuatan(clearDataset.get(i).get(j));
                        break;
                    case 13:
                        allData.setGaransi(Integer.toString(clearDataset.get(i).get(j)));
                        break;
                    case 14:
                        allData.setStatus_perangkat(Integer.toString(clearDataset.get(i).get(j)));
                        break;
                    case 15:
                        allData.setTahun_awal_operasi(clearDataset.get(i).get(j));
                        break;
                    case 16:
                        allData.setOwner(Integer.toString(clearDataset.get(i).get(j)));
                        break;
                    case 17:
                        allData.setPengelola(Integer.toString(clearDataset.get(i).get(j)));
                        break;
                    case 18:
                        allData.setNip(Integer.toString(clearDataset.get(i).get(j)));
                        break;
                    case 19:
                        allData.setNama(Integer.toString(clearDataset.get(i).get(j)));
                        break;
                    case 20:
                        allData.setEmail(Integer.toString(clearDataset.get(i).get(j)));
                        break;
                    case 21:
                        allData.setDeskripsi(Integer.toString(clearDataset.get(i).get(j)));
                        break;
                    case 22:
                        allData.setNama_bidang(Integer.toString(clearDataset.get(i).get(j)));
                        break;
                }

            }
            dataset.add(allData);
        }
        return dataset;
    }

    public ArrayList<Integer> getVariable() {
        KMeanSetting setting = kMeanSettingRepository.getDataSetting();
        return parsingVariable(setting.getVariable());
    }

    private ArrayList<Integer> parsingVariable(String st) {
//        String num = "22,33,44,55,66,77";
        String str[] = st.split(",");
        List<String> al = new ArrayList<String>();
        al = Arrays.asList(str);

        ArrayList<Integer> variable = new ArrayList<>();
        for (int i = 0; i < al.size(); i++) {
            variable.add(Integer.parseInt(al.get(i)));
        }

        return variable;
    }

    private int getDataValue(int valueIndex, String Data) {
        List<String> list = new ArrayList<>();
        int val = -1;
        switch (valueIndex) {
            case 1:
                list = perangkatRepository.getJenisPC();
                val = getIndex(list, Data);
                break;
            case 2:
                list = perangkatRepository.getMerk();
                val = getIndex(list, Data);
                break;
            case 3:
                list = perangkatRepository.getType();
                val = getIndex(list, Data);
                break;
            case 4:
//                list = perangkatRepository.getProsesor();
                if (Data.toUpperCase().equals("CORE I5")) {
                    val = 5;
                } else if (Data.toUpperCase().equals("CORE I7")) {
                    val = 7;
                } else if (Data.toUpperCase().equals("CORE I9")) {
                    val = 9;
                } else if (Data.toUpperCase().equals("CORE I3")) {
                    val = 3;
                } else if (Data.toUpperCase().contains("DUAL")) {
                    val = 2;
                } else if (Data.toUpperCase().contains("PENTIUM")) {
                    val = 1;
                } else if (Data.toUpperCase().contains("RYZEN")) {
                    val = 5;
                } else {
                    val = 1;
                }
                break;
            case 5:
                if (Data.toUpperCase().contains("10")) {
                    val = 10;
                } else if (Data.toUpperCase().contains("7")) {
                    val = 7;
                } else if (Data.toUpperCase().contains("XP")) {
                    val = 3;
                } else if (Data.toUpperCase().contains("LINUX")) {
                    val = 5;
                } else {
                    val = 1;
                }
                break;
            case 6:
//                list = perangkatRepository.get();
                val = Integer.parseInt(Data);
                break;
            case 7:
//                list = perangkatRepository.get();
                val = Integer.parseInt(Data);
                break;
            case 8:
                list = perangkatRepository.getVGA();
                val = getIndex(list, Data);
                break;
            case 9:
//                list = perangkatRepository.getMonitor();
//                val = getIndex(list, Data);
                try {
                val = Integer.parseInt(Data.substring(0, 2));
            } catch (Exception e) {
                val = 12;
            }
            break;
            case 10:
                list = perangkatRepository.getAntarmuka();
                val = getIndex(list, Data);
                break;
            case 11:
                list = perangkatRepository.getMACAddress();
                val = getIndex(list, Data);
                break;
            case 12:
                val = Integer.parseInt(Data);
//                val = getIndex(list, Data);
                break;
            case 13:
//                list = perangkatRepository.getGaransi();
//                val = getIndex(list, Data);
                if (Data.toUpperCase().contains("EXPIRED")) {
                    val = 0;
                } else {
                    try {
                        val = Integer.parseInt(Data.substring(0, 1));
                    } catch (Exception e) {
                        val = 1;
                    }
                }
                break;
            case 14:
//                list = perangkatRepository.getStatusPerangkat();
//                val = getIndex(list, Data);
                if (Data.toUpperCase().equals("BELI")) {
                    val = 10;
                } else if (Data.toUpperCase().equals("SEWA")) {
                    val = 5;
                } else {
                    val = 2;
                }
                break;
            case 15:
//                list = perangkatRepository.getTahunAwalOperasi();
                val = Integer.parseInt(Data);
                break;
            case 16:
                list = perangkatRepository.getOwner();
                val = getIndex(list, Data);
                break;
            case 17:
                list = perangkatRepository.getPengelola();
                val = getIndex(list, Data);
                break;
            case 18:
                list = perangkatRepository.getNip();
                val = getIndex(list, Data);
                break;
            case 19:
                list = perangkatRepository.getNamaPengguna();
                val = getIndex(list, Data);
                break;
            case 20:
                list = perangkatRepository.getEmailPengguna();
                val = getIndex(list, Data);
                break;
            case 21:
                list = perangkatRepository.getDeskripsi();
                val = getIndex(list, Data);
                break;
            case 22:
                list = perangkatRepository.getBidang();
                val = getIndex(list, Data);
                break;
        }
//        if (valueIndex != 6 && valueIndex != 7) {
//            val *= 10;
//        }
        return val;
    }

    private int getIndex(List<String> list, String Data) {
        for (int i = 0; i < list.size(); i++) {
            if (Data.equals(list.get(i))) {
                return i + 1;
            }
        }
        return 0;
    }

    private ArrayList<ArrayList<Integer>> getDataSet(ArrayList<Integer> variable, ArrayList<AllData> rawDataSet) {
        String val = "";

        ArrayList<ArrayList<Integer>> clearDataset = new ArrayList<>();
        for (int i = 0; i < rawDataSet.size(); i++) {
            ArrayList<Integer> dataList = new ArrayList<>();
            for (int j = 0; j < variable.size(); j++) {
                // pilih data sesuai dengan variable yang dipilih
                val = getValueBaseOnVariable(rawDataSet, variable.get(j), i);
                dataList.add(
                        getDataValue(variable.get(j), val)
                );
            }
            clearDataset.add(dataList);

        }

        return clearDataset;
    }

    private String getValueBaseOnVariable(ArrayList<AllData> rawDataSet, int variable, int i) {
        String val = "";
        switch (variable) {
            case 1:
                val = rawDataSet.get(i).getJenis();
                break;
            case 2:
                val = rawDataSet.get(i).getMerk();
                break;
            case 3:
                val = rawDataSet.get(i).getType();
                break;
            case 4:
                val = rawDataSet.get(i).getProsesor();
                break;
            case 5:
                val = rawDataSet.get(i).getOs();
                break;
            case 6:
                val = Integer.toString(rawDataSet.get(i).getRam());
                break;
            case 7:
                val = Integer.toString(rawDataSet.get(i).getHdd());
                break;
            case 8:
                val = rawDataSet.get(i).getVga();
                break;
            case 9:
                val = rawDataSet.get(i).getMonitor();
                break;
            case 10:
                val = rawDataSet.get(i).getAntarmuka();
                break;
            case 11:
                val = rawDataSet.get(i).getMac_address();
                break;
            case 12:
                val = Integer.toString(rawDataSet.get(i).getTahun_pembuatan());
                break;
            case 13:
                val = rawDataSet.get(i).getGaransi();
                break;
            case 14:
                val = rawDataSet.get(i).getStatus_perangkat();
                break;
            case 15:
                val = Integer.toString(rawDataSet.get(i).getTahun_awal_operasi());
                break;
            case 16:
                val = rawDataSet.get(i).getOwner();
                break;
            case 17:
                val = rawDataSet.get(i).getPengelola();
                break;
            case 18:
                val = rawDataSet.get(i).getNip();
                break;
            case 19:
                val = rawDataSet.get(i).getNama();
                break;
            case 20:
                val = rawDataSet.get(i).getEmail();
                break;
            case 21:
                val = rawDataSet.get(i).getDeskripsi();
                break;
            case 22:
                val = rawDataSet.get(i).getNama_bidang();
                break;
        }
        if (val.equals("")) {
            val = "0";
        }
        return val;
    }

    private ArrayList<AllData> getRawDataset() {
        ArrayList<AllData> rawData = new ArrayList<>();
        List<Bidang> bidangs = bidangRepository.findAll();
        List<Pengguna> penggunas = penggunaRepository.findAll();
        List<Pemilik> pemiliks = pemilikRepository.findAll();
        List<Perangkat> perangkats = perangkatRepository.getAllDataOrderByID();
        List<KMeanCluster> kMeanClusters = kMeanClusterRepository.getDataByIdASC();
        for (int i = 0; i < perangkats.size(); i++) {
            AllData allData = new AllData();
            allData.setId(perangkats.get(i).getId());
            allData.setJenis(perangkats.get(i).getJenis());
            allData.setMerk(perangkats.get(i).getMerk());
            allData.setType(perangkats.get(i).getType());
            allData.setProsesor(perangkats.get(i).getProsesor());
            allData.setOs(perangkats.get(i).getOs());
            allData.setRam(Integer.parseInt(perangkats.get(i).getRam()));
            allData.setHdd(Integer.parseInt(perangkats.get(i).getHdd()));
            allData.setVga(perangkats.get(i).getVga());
            allData.setMonitor(perangkats.get(i).getMonitor());
            allData.setAntarmuka(perangkats.get(i).getAntarmuka());
            allData.setMac_address(perangkats.get(i).getMac_address());
            allData.setTahun_pembuatan(perangkats.get(i).getTahun_pembuatan());
            allData.setGaransi(perangkats.get(i).getGaransi());
            allData.setStatus_perangkat(perangkats.get(i).getStatus_perangkat());
            allData.setTahun_awal_operasi(perangkats.get(i).getTahun_awal_operasi());
            allData.setOwner(
                    pemiliks.get(
                            getOwnerId(pemiliks, perangkats.get(i).getIdOwner())
                    ).getNama()
            );
            allData.setPengelola(
                    pemiliks.get(
                            getOwnerId(pemiliks, perangkats.get(i).getIdOwner())
                    ).getPengelola()
            );
            allData.setNip(
                    penggunas.get(
                            getUserId(penggunas, perangkats.get(i).getIdPengguna())
                    ).getNip()
            );
            allData.setNama(
                    penggunas.get(
                            getUserId(penggunas, perangkats.get(i).getIdPengguna())
                    ).getNama()
            );
            allData.setEmail(
                    penggunas.get(
                            getUserId(penggunas, perangkats.get(i).getIdPengguna())
                    ).getEmail()
            );
            allData.setDeskripsi(
                    penggunas.get(
                            getUserId(penggunas, perangkats.get(i).getIdPengguna())
                    ).getDeskripsi()
            );
            allData.setNama_bidang(
                    bidangs.get(
                            getBidangId(bidangs, penggunas.get(
                                    getUserId(penggunas, perangkats.get(i).getIdPengguna())
                            ).getBidang()
                            )
                    ).getNama_bidang()
            );
            allData.setCluster(
                    kMeanClusters.get(
                            getClusterId(kMeanClusters, perangkats.get(i).getIdCluster())
                    ).getCurent_cluster()
            );

            allData.setStatus_cluster(
                    kMeanClusters.get(
                            getClusterId(kMeanClusters, perangkats.get(i).getIdCluster())
                    ).getStatus()
            );
            allData.setCluster_recomended(
                    kMeanClusters.get(
                            getClusterId(kMeanClusters, perangkats.get(i).getIdCluster())
                    ).getRecomended_cluster()
            );

            rawData.add(allData);
        }

        return rawData;

    }

    public Map<String, Boolean> createCategory() {
        Map<String, Boolean> status = new HashMap<>();
        int lowRank = 0;
        int HightRank = 0;
        int findfirst = 0;
        try {

            List<KMeanCluster> data = kMeanClusterRepository.getDataByIdASC();
            List<Integer> cluster = kMeanClusterRepository.getCluster();
            List<Perangkat> perangkat = perangkatRepository.getAllDataOrderByID();
            List<Integer> newClusterCAtegory = new ArrayList<>();
            List<Integer> minRank = new ArrayList<>();
            List<Integer> maxRank = new ArrayList<>();

            List<ClusteringPerangkat> clusteringPerangkats = new ArrayList<>();

//            searching for low rank
            for (int i = 0; i < cluster.size(); i++) {

//                int idx = 0;
                for (int j = 0; j < data.size(); j++) {
                    if (data.get(j).getRecomended_cluster() == cluster.get(i)) {
                        if (findfirst == 0) {
                            lowRank = j;
                            findfirst++;
                        }
//                        System.out.println("MIN MATCH ON CLUSTER -> "+cluster.get(i));
                        if ((getProccessorRank(perangkat.get(lowRank).getProsesor()) <= getProccessorRank(perangkat.get(j).getProsesor()))
                                && (Integer.parseInt(perangkat.get(lowRank).getRam()) <= Integer.parseInt(perangkat.get(j).getRam()))
                                && (Integer.parseInt(perangkat.get(lowRank).getHdd()) <= Integer.parseInt(perangkat.get(j).getHdd()))
                                && (getOSRank(perangkat.get(lowRank).getOs()) <= getOSRank(perangkat.get(j).getOs()))) {
                            //null
                        } else {
                            lowRank = j;
                        }
                    }

                }
                minRank.add(lowRank);
                lowRank = 0;
                findfirst = 0;
            }
            System.out.println("MIN SPEC PER CLUSTER");
            for (int i = 0; i < minRank.size(); i++) {
                System.out.println("CLUSTER - " + i);
                System.out.println(perangkat.get(minRank.get(i)).toString());
            }

//            searching hight rank
            for (int i = 0; i < cluster.size(); i++) {

//                int idx = 0;
                for (int j = 0; j < data.size(); j++) {
                    if (data.get(j).getRecomended_cluster() == cluster.get(i)) {
                        if (findfirst == 0) {
                            lowRank = j;
                            findfirst++;
                        }
//                        System.out.println("MIN MATCH ON CLUSTER -> "+cluster.get(i));
                        if ((getProccessorRank(perangkat.get(HightRank).getProsesor()) >= getProccessorRank(perangkat.get(j).getProsesor()))
                                && (Integer.parseInt(perangkat.get(HightRank).getHdd()) >= Integer.parseInt(perangkat.get(j).getHdd()))
                                && (Integer.parseInt(perangkat.get(HightRank).getRam()) >= Integer.parseInt(perangkat.get(j).getRam()))
                                && (getOSRank(perangkat.get(HightRank).getOs()) >= getOSRank(perangkat.get(j).getOs()))) {
                            //null
                        } else {
                            HightRank = j;
                        }
                    }
                }
                maxRank.add(HightRank);
                HightRank = 0;
                findfirst = 0;
            }
            System.out.println("MAXSPEC PER CLUSTER");
            for (int i = 0; i < maxRank.size(); i++) {
                System.out.println("CLUSTER - " + i);
                System.out.println(perangkat.get(maxRank.get(i)).toString());
            }

//            searhcing for standard
            for (int i = 0; i < cluster.size(); i++) {

                int idx = 0;
                for (int j = 0; j < data.size(); j++) {
                    
                    if (data.get(j).getRecomended_cluster() == cluster.get(i)) {
                        if (findfirst == 0) {
                            idx = j;
                            findfirst++;
                        }
                        if (
                                ((perangkat.get(j).getTahun_pembuatan() >= perangkat.get(minRank.get(i)).getTahun_pembuatan())
                                && (perangkat.get(j).getTahun_pembuatan() <= perangkat.get(maxRank.get(i)).getTahun_pembuatan()))
                                && ((getProccessorRank(perangkat.get(j).getProsesor()) >= getProccessorRank(perangkat.get(minRank.get(i)).getProsesor())
                                && (getProccessorRank(perangkat.get(j).getProsesor()) <= getProccessorRank(perangkat.get(maxRank.get(i)).getProsesor()))))
                                && ((getOSRank(perangkat.get(j).getOs()) >= getOSRank(perangkat.get(minRank.get(i)).getOs()))
                                && (getOSRank(perangkat.get(j).getOs()) <= getOSRank(perangkat.get(maxRank.get(i)).getOs())))
                                && ((Integer.parseInt(perangkat.get(j).getRam()) >= Integer.parseInt(perangkat.get(minRank.get(i)).getRam()))
                                && (Integer.parseInt(perangkat.get(HightRank).getRam()) <= Integer.parseInt(perangkat.get(maxRank.get(i)).getRam())))
                                && ((Integer.parseInt(perangkat.get(HightRank).getHdd()) >= Integer.parseInt(perangkat.get(minRank.get(i)).getHdd()))
                                && (Integer.parseInt(perangkat.get(HightRank).getHdd()) <= Integer.parseInt(perangkat.get(maxRank.get(i)).getHdd())))) {
                        } else {
                            idx = j;

                        }
                    }
                }
                newClusterCAtegory.add(idx);
                findfirst = 0;
            }
            
            System.out.println("STANDARD PER CLUSTER");
            for (int i = 0; i < maxRank.size(); i++) {
                System.out.println("TANDARD CLUSTER - " + i);
                System.out.println(perangkat.get(newClusterCAtegory.get(i)).toString());
            }
            
            

            System.out.println("BANYAK KATEGORI : " + newClusterCAtegory.size());
            for (int i = 0; i < newClusterCAtegory.size(); i++) {

                Perangkat p = perangkatRepository.getOneData(
                        perangkat.get(
                                newClusterCAtegory.get(i)
                        ).getId()
                );
                ClusteringPerangkat cp
                        = new ClusteringPerangkat(
                                i,
                                cluster.get(i),
                                "PC/LAPTOP",
                                p.getOs(),
                                p.getProsesor(),
                                Integer.parseInt(p.getRam()),
                                Integer.parseInt(p.getHdd()),
                                p.getMonitor(),
                                "-",
                                p.getAntarmuka(),
                                "-",
                                "-",
                                p.getGaransi());

                clusteringPerangkats.add(cp);

            }

            klasifikasiPerangkatRepository.deleteAll();
            klasifikasiPerangkatRepository.saveAll(clusteringPerangkats);
            status.put("Status", Boolean.TRUE);
        } catch (Exception e) {
            e.printStackTrace();
            status.put("Status", Boolean.FALSE);
        }
        return status;
    }

    public int getOwnerId(List<Pemilik> pemiliks, long searchId) {

        for (int i = 0; i < pemiliks.size(); i++) {
            if (pemiliks.get(i).getId() == searchId) {
                return i;
            }
        }
        return -1;
    }

    public int getUserId(List<Pengguna> penggunas, long searchId) {
        for (int i = 0; i < penggunas.size(); i++) {
            if (penggunas.get(i).getId() == searchId) {
                return i;
            }
        }
        return -1;
    }

    public int getBidangId(List<Bidang> bidangs, long searchId) {
        for (int i = 0; i < bidangs.size(); i++) {
            if (bidangs.get(i).getId() == searchId) {
                return i;
            }
        }
        return -1;
    }

    public int getClusterId(List<KMeanCluster> cluster, long searchId) {
        for (int i = 0; i < cluster.size(); i++) {
            if (cluster.get(i).getId() == searchId) {
                return i;
            }
        }
        return -1;
    }

    private int getProccessorRank(String Data) {
        int val = -1;
        if (Data.toUpperCase().equals("CORE I5")) {
            val = 5;
        } else if (Data.toUpperCase().equals("CORE I7")) {
            val = 7;
        } else if (Data.toUpperCase().equals("CORE I9")) {
            val = 9;
        } else if (Data.toUpperCase().equals("CORE I3")) {
            val = 3;
        } else if (Data.toUpperCase().contains("DUAL")) {
            val = 2;
        } else if (Data.toUpperCase().contains("PENTIUM")) {
            val = 1;
        } else if (Data.toUpperCase().contains("RYZEN")) {
            val = 5;
        } else {
            val = 1;
        }
        return val;
    }

    private int getOSRank(String Data) {
        int val = -1;

        if (Data.toUpperCase().contains("10")) {
            val = 10;
        } else if (Data.toUpperCase().contains("7")) {
            val = 7;
        } else if (Data.toUpperCase().contains("XP")) {
            val = 3;
        } else if (Data.toUpperCase().contains("LINUX")) {
            val = 5;
        } else {
            val = 1;
        }
        return val;
    }

    private int getMonitorRank(String data) {
        try {

            return Integer.parseInt(data.substring(0, 2));

        } catch (Exception e) {
            return 12;
        }

    }

}
