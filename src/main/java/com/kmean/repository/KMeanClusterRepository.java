/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kmean.repository;

import com.kmean.model.KMeanCluster;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Centry
 */
@Repository
public interface KMeanClusterRepository extends JpaRepository<KMeanCluster, Long>{
    
    
    @Query(value = "SELECT * FROM kmean_cluster ORDER BY id ASC", nativeQuery = true)
    public List<KMeanCluster> getDataByIdASC();
    
    @Query(value = "SELECT DISTINCT recomended_cluster FROM kmean_cluster", nativeQuery = true)
    public List<Integer> getCluster(); 
    
    @Query(value = "SELECT MAX(recomended_cluster) FROM kmean_cluster", nativeQuery = true)
    public int getTotalCluster();
    
    @Query(value = "    SELECT COUNT(DISTINCT curent_cluster) FROM kmean_cluster",nativeQuery = true)
    public int getTotalUsedClusterBefore();
}
