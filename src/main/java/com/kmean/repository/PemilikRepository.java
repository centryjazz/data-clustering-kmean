/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kmean.repository;

import com.kmean.model.Pemilik;
import com.kmean.model.Perangkat;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Centry
 */
@Repository
public interface PemilikRepository extends JpaRepository<Pemilik, Long>{
    
    @Query(value = "select * from pemilik where id = ?1",nativeQuery = true)
    Pemilik getOneData(long id);
    
}
