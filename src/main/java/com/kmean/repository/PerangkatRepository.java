/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kmean.repository;

import com.kmean.model.Perangkat;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Centry
 */
@Repository
public interface PerangkatRepository extends JpaRepository<Perangkat, Long> {

    List<Perangkat> findByidOwner(long id);
    
    @Query(value = "select * from perangkat where id_pengguna = ?1",nativeQuery = true)
    Perangkat getOneData(long id);
    
    List<Perangkat> findByidPengguna(long id);
    
    @Query(value = "SELECT * FROM perangkat ORDER BY id ASC",nativeQuery = true)
    List<Perangkat> getAllDataOrderByID();

    @Query(value = "SELECT DISTINCT jenis FROM perangkat", nativeQuery = true)
    List<String> getJenisPC();

    @Query(value = "SELECT DISTINCT merk FROM perangkat", nativeQuery = true)
    List<String> getMerk();

    @Query(value = "SELECT DISTINCT type FROM perangkat", nativeQuery = true)
    List<String> getType();

    @Query(value = "SELECT DISTINCT prosesor FROM perangkat", nativeQuery = true)
    List<String> getProsesor();

    @Query(value = "SELECT DISTINCT os FROM perangkat", nativeQuery = true)
    List<String> getOS();

//    @Query(value = "SELECT DISTINCT ram FROM perangkat", nativeQuery = true)
//    List<String> getRAM();
//
//    @Query(value = "SELECT DISTINCT hdd FROM perangkat", nativeQuery = true)
//    List<String> getHDD();

    @Query(value = "SELECT DISTINCT vga FROM perangkat", nativeQuery = true)
    List<String> getVGA();
    
    @Query(value = "SELECT DISTINCT monitor FROM perangkat", nativeQuery = true)
    List<String> getMonitor();
    
    @Query(value = "SELECT DISTINCT antarmuka FROM perangkat", nativeQuery = true)
    List<String> getAntarmuka();
    
    @Query(value = "SELECT DISTINCT mac_address FROM perangkat", nativeQuery = true)
    List<String> getMACAddress();
    
    @Query(value = "SELECT DISTINCT tahun_pembuatan FROM perangkat", nativeQuery = true)
    List<String> getTahunPembuatan();
    
    @Query(value = "SELECT DISTINCT garansi FROM perangkat", nativeQuery = true)
    List<String> getGaransi();
    
    @Query(value = "SELECT DISTINCT status_perangkat FROM perangkat", nativeQuery = true)
    List<String> getStatusPerangkat();
    
    @Query(value = "SELECT DISTINCT tahun_awal_operasi FROM perangkat", nativeQuery = true)
    List<String> getTahunAwalOperasi();
    
    @Query(value = "SELECT nama FROM pemilik", nativeQuery = true)
    List<String> getOwner();
    
    @Query(value = "SELECT pengelola FROM pemilik", nativeQuery = true)
    List<String> getPengelola();
    
    @Query(value = "SELECT DISTINCT nip FROM pengguna", nativeQuery = true)
    List<String> getNip();
    
    @Query(value = "SELECT DISTINCT nama FROM pengguna", nativeQuery = true)
    List<String> getNamaPengguna();
    
    @Query(value = "SELECT DISTINCT email FROM pengguna", nativeQuery = true)
    List<String> getEmailPengguna();
    
    @Query(value = "SELECT DISTINCT deskripsi FROM pengguna", nativeQuery = true)
    List<String> getDeskripsi();
    
    @Query(value = "SELECT DISTINCT nama_bidang FROM bidang ORDER BY id ASC", nativeQuery = true)
    List<String> getBidang();
    
    @Query(value = "SELECT id from perangkat order by id ASC", nativeQuery = true)
    List<Long> getIdList();
}
