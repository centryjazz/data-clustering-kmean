/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kmean.repository;

import com.kmean.model.KMeanSetting;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Centry
 */
@Repository
public interface KMeanSettingRepository extends JpaRepository<KMeanSetting, Long>{
    
    @Query(value = "SELECT * FROM kmean_setting LIMIT 1", nativeQuery = true)
    KMeanSetting getDataSetting();
}
