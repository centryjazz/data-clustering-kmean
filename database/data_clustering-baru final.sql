/*
SQLyog Ultimate v12.5.1 (32 bit)
MySQL - 10.4.11-MariaDB : Database - kmean_clustering
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`kmean_clustering` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `kmean_clustering`;

/*Table structure for table `bidang` */

DROP TABLE IF EXISTS `bidang`;

CREATE TABLE `bidang` (
  `id` varchar(255) NOT NULL,
  `nama_bidang` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `bidang` */

insert  into `bidang`(`id`,`nama_bidang`) values 
('42','PEMASARAN'),
('50','KSA'),
('51','KONSTRUKSI'),
('55','K2K3'),
('64','PENGADAAN'),
('66','NIAGA'),
('69','TRANSAKSI ENERGI'),
('70','JARINGAN'),
('73','PERENCANAAN'),
('83','FUNGSIONAL AHLI'),
('99','MANAGER');

/*Table structure for table `hibernate_sequence` */

DROP TABLE IF EXISTS `hibernate_sequence`;

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `hibernate_sequence` */

insert  into `hibernate_sequence`(`next_val`) values 
(314),
(314),
(314),
(314);

/*Table structure for table `klasifikasi_perangkat` */

DROP TABLE IF EXISTS `klasifikasi_perangkat`;

CREATE TABLE `klasifikasi_perangkat` (
  `id` varchar(255) NOT NULL,
  `klasifikasi` int(11) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `os` varchar(255) DEFAULT NULL,
  `prosesor` varchar(255) DEFAULT NULL,
  `ram` int(11) DEFAULT NULL,
  `hdd` int(11) DEFAULT NULL,
  `monitor` varchar(255) DEFAULT NULL,
  `wifi` varchar(255) DEFAULT NULL,
  `interface` varchar(255) DEFAULT NULL,
  `mouse` varchar(255) DEFAULT NULL,
  `keyboard` varchar(255) DEFAULT NULL,
  `garansi` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `klasifikasi_perangkat` */

insert  into `klasifikasi_perangkat`(`id`,`klasifikasi`,`category`,`os`,`prosesor`,`ram`,`hdd`,`monitor`,`wifi`,`interface`,`mouse`,`keyboard`,`garansi`) values 
('312',1,'PC/LAPTOP','WINDOWS 7 ENTERPRISE 64BIT','CORE i3',2,500,'18.5\" (Non Touch Screen)','-','USB / HDMI / VGA','-','-','Expired'),
('313',2,'PC/LAPTOP','WINDOWS 7 ENTERPRISE 64BIT','CORE i3',4,1000,'18.5\" (Non Touch Screen)','-','USB / HDMI / VGA','-','-','Expired');

/*Table structure for table `kmean_cluster` */

DROP TABLE IF EXISTS `kmean_cluster`;

CREATE TABLE `kmean_cluster` (
  `id` varchar(255) NOT NULL,
  `curent_cluster` varchar(255) DEFAULT NULL,
  `recomended_cluster` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `kmean_cluster` */

insert  into `kmean_cluster`(`id`,`curent_cluster`,`recomended_cluster`,`status`) values 
('1','0','1','UPGRADE'),
('10','2','2','STANDARD'),
('100','2','1','DOWN GRADE'),
('101','2','1','DOWN GRADE'),
('11','2','2','STANDARD'),
('12','2','2','STANDARD'),
('13','2','1','DOWN GRADE'),
('14','2','1','DOWN GRADE'),
('15','2','1','DOWN GRADE'),
('16','2','1','DOWN GRADE'),
('17','2','1','DOWN GRADE'),
('18','2','1','DOWN GRADE'),
('19','2','1','DOWN GRADE'),
('2','2','1','DOWN GRADE'),
('20','2','1','DOWN GRADE'),
('21','2','2','STANDARD'),
('22','2','1','DOWN GRADE'),
('23','2','1','DOWN GRADE'),
('24','2','1','DOWN GRADE'),
('25','2','1','DOWN GRADE'),
('26','2','1','DOWN GRADE'),
('27','2','1','DOWN GRADE'),
('28','2','1','DOWN GRADE'),
('29','2','1','DOWN GRADE'),
('3','2','1','DOWN GRADE'),
('30','2','1','DOWN GRADE'),
('301','2','1','DOWN GRADE'),
('31','2','1','DOWN GRADE'),
('32','2','1','DOWN GRADE'),
('33','2','2','STANDARD'),
('34','2','1','DOWN GRADE'),
('35','2','1','DOWN GRADE'),
('36','2','2','STANDARD'),
('37','2','1','DOWN GRADE'),
('38','2','1','DOWN GRADE'),
('39','2','1','DOWN GRADE'),
('4','2','1','DOWN GRADE'),
('40','2','1','DOWN GRADE'),
('41','2','1','DOWN GRADE'),
('42','2','1','DOWN GRADE'),
('43','2','1','DOWN GRADE'),
('44','2','1','DOWN GRADE'),
('45','2','2','STANDARD'),
('46','2','1','DOWN GRADE'),
('47','2','2','STANDARD'),
('48','2','1','DOWN GRADE'),
('49','2','1','DOWN GRADE'),
('5','2','1','DOWN GRADE'),
('50','2','1','DOWN GRADE'),
('51','2','1','DOWN GRADE'),
('52','2','1','DOWN GRADE'),
('53','2','1','DOWN GRADE'),
('54','2','1','DOWN GRADE'),
('55','2','1','DOWN GRADE'),
('56','2','1','DOWN GRADE'),
('57','2','1','DOWN GRADE'),
('58','2','1','DOWN GRADE'),
('59','2','1','DOWN GRADE'),
('6','2','1','DOWN GRADE'),
('60','2','1','DOWN GRADE'),
('61','2','1','DOWN GRADE'),
('62','2','1','DOWN GRADE'),
('63','2','1','DOWN GRADE'),
('64','2','1','DOWN GRADE'),
('65','2','1','DOWN GRADE'),
('66','2','1','DOWN GRADE'),
('67','2','1','DOWN GRADE'),
('68','2','1','DOWN GRADE'),
('69','2','1','DOWN GRADE'),
('7','2','1','DOWN GRADE'),
('70','2','1','DOWN GRADE'),
('71','2','1','DOWN GRADE'),
('72','2','1','DOWN GRADE'),
('73','2','1','DOWN GRADE'),
('74','2','1','DOWN GRADE'),
('75','2','1','DOWN GRADE'),
('76','2','1','DOWN GRADE'),
('77','2','1','DOWN GRADE'),
('78','2','1','DOWN GRADE'),
('79','2','1','DOWN GRADE'),
('8','2','1','DOWN GRADE'),
('80','2','1','DOWN GRADE'),
('81','2','1','DOWN GRADE'),
('82','2','1','DOWN GRADE'),
('83','2','1','DOWN GRADE'),
('84','2','1','DOWN GRADE'),
('85','2','1','DOWN GRADE'),
('86','2','1','DOWN GRADE'),
('87','2','1','DOWN GRADE'),
('88','2','1','DOWN GRADE'),
('89','2','1','DOWN GRADE'),
('9','2','1','DOWN GRADE'),
('90','2','1','DOWN GRADE'),
('91','2','1','DOWN GRADE'),
('92','2','1','DOWN GRADE'),
('93','2','1','DOWN GRADE'),
('94','2','1','DOWN GRADE'),
('95','2','1','DOWN GRADE'),
('96','2','1','DOWN GRADE'),
('97','2','1','DOWN GRADE'),
('98','2','1','DOWN GRADE'),
('99','2','1','DOWN GRADE');

/*Table structure for table `kmean_setting` */

DROP TABLE IF EXISTS `kmean_setting`;

CREATE TABLE `kmean_setting` (
  `id` varchar(255) NOT NULL,
  `cluster` int(11) DEFAULT NULL,
  `treshold` double DEFAULT NULL,
  `variable` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `kmean_setting` */

insert  into `kmean_setting`(`id`,`cluster`,`treshold`,`variable`) values 
('1',3,0.8,'1,4,5,6,7,8,9,10,12,13,14,15,16,17,19,22');

/*Table structure for table `pemilik` */

DROP TABLE IF EXISTS `pemilik`;

CREATE TABLE `pemilik` (
  `id` varchar(255) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `pengelola` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `pemilik` */

insert  into `pemilik`(`id`,`nama`,`pengelola`) values 
('1','PT ICON+','UID BANTEN'),
('2','UP3 CIKOKOL','UP3 CIKOKOL');

/*Table structure for table `pengguna` */

DROP TABLE IF EXISTS `pengguna`;

CREATE TABLE `pengguna` (
  `id` varchar(255) NOT NULL,
  `nip` varchar(20) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `deskripsi` varchar(255) DEFAULT NULL,
  `bidang` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bidang` (`bidang`),
  CONSTRAINT `pengguna_ibfk_1` FOREIGN KEY (`bidang`) REFERENCES `bidang` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `pengguna` */

insert  into `pengguna`(`id`,`nip`,`nama`,`email`,`deskripsi`,`bidang`) values 
('1','','PETER SIREGAR','peter.siregar@pln.co.id','','70'),
('10','','SYAIFULLAH ARSYAD','syaifullah.arsyad@pln.co.id','','70'),
('100','','ANDI AULIA PUTRA','andi.aulia@pln.co.id','Manager Bidang 2','73'),
('101','','MUHAMMAD AZZAT KHAN','azzat.khan@pln.co.id','Staff Mapping','73'),
('11','','SASONGKO ARIBOWO','sasongko.aribowo@pln.co.id','','70'),
('12','','MAHADHIR','mahadhir@pln.co.id','','70'),
('13','','ASEP SUPRIYANTO','asep.supriyanto@pln.co.id','','70'),
('14','','DEDIN SAPARUDIN','dedin.saparudin@pln.co.id','','70'),
('15','','YUDHA EKO PRASETYO','yudha.prasetyo@pln.co.id','','70'),
('16','','ANDI AULIA PUTRA','andi.aulia@pln.co.id','','73'),
('17','','FUAD HASAN NAWAWI','fuad.hasan@pln.co.id','','73'),
('18','','DORA SEVMARMILA','dora.sevmarmila@pln.co.id','','73'),
('19','','KHAIRUL RIDHA','khairul.ridha@pln.co.id','','73'),
('2','','YOLANDA GUSTIAN','yolanda.gustian@pln.co.id','','70'),
('20','','ENJANG RAHMAN','enjang.rahman@pln.co.id','','73'),
('21','','FADIYA FIKROH NASYIROH','fadiya.fikroh@pln.co.id','','73'),
('22','','MUHAMMAD AZZAT KHAN','azzat.khan@pln.co.id','','73'),
('23','','FAUZIE','','Outsourcing','73'),
('24','','RIMA ANGGRAINI','','Outsourcing','73'),
('25','','INTAN MEILANI','','Outsourcing','73'),
('26','','JEPRI RAHMAT','','Outsourcing','73'),
('27','','MURDIN','murdin@pln.co.id','','83'),
('28','','ADING SURYADI','ading.suryadi@pln.co.id','','83'),
('29','','ANSORIZAL','ansorizal@pln.co.id','','83'),
('3','','MURTANI','murtani@pln.co.id','','70'),
('30','','HARTO','harto@pln.co.id','','83'),
('301','nip','fajar salam','email','','42'),
('31','','YADI SUPRIYADI','yadi.supriyadi@pln.co.id','','83'),
('32','','YULIADI','yuliadi@pln.co.id','','83'),
('33','','HARI NADIRI SULISTIYONO','hari.sulistiyono@pln.co.id','','83'),
('34','','WILY SILVIYANTY','wily.silviyanty@pln.co.id','','64'),
('35','','HAERUL SALEH','haerul.saleh@pln.co.id','','64'),
('36','','RADEN RAMA PRAMUDYA KUSUMA','raden.kusuma@pln.co.id','','50'),
('37','','RIANI MASNAPITA SIHOMBING','riani.sihombing@pln.co.id','','50'),
('38','','MUAMMAR ARAFAT','muammar.arafat@pln.co.id','','50'),
('39','','MOHAMMAD ARIF FURHAN','moh.arif.furhan@pln.co.id','','50'),
('4','','ISA CANDI BIMO SAKTI','isa.sakti@pln.co.id','','70'),
('40','','KOKOM KOHAROCHMAH','kokom.koharochmah@pln.co.id','','50'),
('41','','SATRIA PRIBADI','satria.pribadi@pln.co.id','','50'),
('42','','AZIZAH KUSUMANINGRUM','azizah.kusumaningrum@pln.co.id','','50'),
('43','','ARYA BAGUS SANJAYA','arya.bagus@pln.co.id','','55'),
('44','','EDY MUKTASIM BILLAH','edy.muktasim@pln.co.id','','99'),
('45','','BAHNIARNO','bahniarno@pln.co.id','','51'),
('46','','ADI FITRIATMOJO','adi.fitriatmojo@pln.co.id','','51'),
('47','','NANANG DWI SUKMONO','nanang.ds@pln.co.id','','51'),
('48','','JUDI PAMUNGKAS','judi.pamungkas@pln.co.id','','51'),
('49','','ILHAM ABDILAH','ilham.abdilah@pln.co.id','','51'),
('5','','DESYTA AYU PANGESTIKA','destya.pangestika@pln.co.id','','70'),
('50','','PARIASTUTI','pariastuti@pln.co.id','','51'),
('51','','LUH AYU PRADYANI','ayu.pradyani@pln.co.id','','51'),
('52','','DWIEN CATRA CHRISANDY','catra.chrisandy@pln.co.id','','51'),
('53','','JAENUDIN','jaenudin@pln.co.id','','51'),
('54','','AGUS SUNARYO','agus.sunaryo@pln.co.id','','51'),
('55','','DIAN HARDIANA','dian.hardiana@pln.co.id','','51'),
('56','','ZAKIAH CUT MUTIA','','Outsourcing','51'),
('57','','NURLIANI','','Outsourcing','51'),
('58','','MUHAMMAD RIDUAN','muhammad.riduan@pln.co.id','','51'),
('59','','AGUS SULAEMAN','agus.sulaeman67m@pln.co.id','','51'),
('6','','CYBERLOCK','','Komputer Cyberlock Buka Akses Kunci Gardu','70'),
('60','','HERU EKO PRAYITNO','heru.prayitno@pln.co.id','','66'),
('61','','RATNA PERTIWI','ratna.pertiwi@pln.co.id','','66'),
('62','','ERLIN WULAN SARI','erlin.wulan.sari@pln.co.id','','66'),
('63','','ARYANDRA ANDARU','aryandra.andaru@pln.co.id','','66'),
('64','','HERDI RISKY PARTIARTANTO','herdi.risky@pln.co.id','','66'),
('65','','VINCENTIUS AGUNG PRIBADI','vincentius.AP@pln.co.id','','66'),
('66','','RIZKI NUR AULIA','nur.aulia@pln.co.id','','66'),
('67','','BIANDA TALITHA UTARI AYU','bianda.ayu@pln.co.id','','66'),
('68','','KETY HELKI BIANG','kety.helki@pln.co.id','','66'),
('69','','ARIS MUNANDAR','aris.munandar@pln.co.id','','66'),
('7','','HASIAN EFRAVODITUS SITORUS','hasian.efravoditus@pln.co.id','','70'),
('70','','AZIZAH','azizah@pln.co.id','','66'),
('71','','FRONT LINEAR 1','','Outsourcing','66'),
('72','','FRONT LINEAR 2','','Outsourcing','66'),
('73','','HERI DWI SULISTYO','heri.dwi@pln.co.id ','','69'),
('74','','M. GUNAWAN SATHORI MANGULUANG','gunawan.sathori@pln.co.id','','69'),
('75','','IRWANIDA','irwanida@pln.co.id','','69'),
('76','','KUKUH RADIYANTO','kukuh.radiyanto@pln.co.id','','69'),
('77','','ADILLA SYAMSA','adilla.syamsa@pln.co.id','Test bidang','69'),
('78','','PIAN SUHARDIANSYAH','pian.suhardiansyah@pln.co.id','','69'),
('79','','FADLI ARISTIYAN','fadli.aristiyan@pln.co.id','','69'),
('8','','YUDHA DWI PAMBUDI','yudha.dwi.pambudi@pln.co.id','','70'),
('80','','RINANTO DEMI NUGROHO','rinanto.nugroho@pln.co.id','','69'),
('81','','NI NYOMAN WIRATI','ni.nyoman.wirati@pln.co.id','','69'),
('82','','TEGUH HAQIQI','teguh.haqiqi@pln.co.id','','69'),
('83','','ANGGA PRADANA','angga.pradana@pln.co.id','','69'),
('84','','JUWITA RESTIANISA','juwita.restianisa@pln.co.id','','69'),
('85','','AMR1','','Outsourcing','69'),
('86','','AMR2','','Outsourcing','69'),
('87','','AMR3','','Outsourcing','69'),
('88','','CATER1','','Outsourcing','69'),
('89','','CATER2','','Outsourcing','69'),
('9','','FAIZAL FIKRY','faizal.fikry@pln.co.id','','70'),
('90','','CATER3','','Outsourcing','69'),
('91','','P2TL1','','Outsourcing','69'),
('92','','P2TL2','','Outsourcing','69'),
('93','','P2TL3','','Outsourcing','69'),
('94','','SASONGKO ARIBOWO','sasongko.aribowo@pln.co.id','Manager Bidang 1','70'),
('95','','NANANG DWI SUKMONO','nanang.ds@pln.co.id','Manager Bidang 8','51'),
('96','','M. GUNAWAN SATHORI MANGULUANG','gunawan.sathori@pln.co.id','Manager Bidang 10','69'),
('97','','HERU EKO PRAYITNO','heru.prayitno@pln.co.id','Manager Bidang 9','66'),
('98','','AZIZAH','azizah@pln.co.id','Manager Bidang 11','42'),
('99','','AZIZAH KUSUMANINGRUM','azizah.kusumaningrum@pln.co.id','Manager Bidang 5','50');

/*Table structure for table `perangkat` */

DROP TABLE IF EXISTS `perangkat`;

CREATE TABLE `perangkat` (
  `id` varchar(255) NOT NULL,
  `jenis` varchar(255) DEFAULT NULL,
  `merk` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `prosesor` varchar(255) DEFAULT NULL,
  `os` varchar(255) DEFAULT NULL,
  `ram` int(11) DEFAULT NULL,
  `hdd` int(11) DEFAULT NULL,
  `vga` varchar(255) DEFAULT NULL,
  `monitor` varchar(255) DEFAULT NULL,
  `antarmuka` varchar(255) DEFAULT NULL,
  `mac_address` varchar(255) DEFAULT NULL,
  `tahun_pembuatan` int(4) DEFAULT NULL,
  `garansi` varchar(255) DEFAULT NULL,
  `status_perangkat` varchar(20) DEFAULT NULL,
  `tahun_awal_operasi` int(4) DEFAULT NULL,
  `id_owner` varchar(255) DEFAULT NULL,
  `id_pengguna` varchar(255) DEFAULT NULL,
  `id_cluster` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_owner` (`id_owner`),
  KEY `id_pengguna` (`id_pengguna`),
  KEY `id_cluster` (`id_cluster`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `perangkat` */

insert  into `perangkat`(`id`,`jenis`,`merk`,`type`,`prosesor`,`os`,`ram`,`hdd`,`vga`,`monitor`,`antarmuka`,`mac_address`,`tahun_pembuatan`,`garansi`,`status_perangkat`,`tahun_awal_operasi`,`id_owner`,`id_pengguna`,`id_cluster`) values 
('1','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','1','1'),
('10','PC','LENOVO BRANDED','THINKCENTRE EDGE 92-1JA','CORE i3','WINDOWS 7 ENTERPRISE 64BIT',8,1000,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2012,'Expired','BELI',2013,'2','10','10'),
('100','LAPTOP','HP','PROBOOK 440 G3','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','14.0\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','100','100'),
('101','LAPTOP','HP','PROBOOK 440 G3','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','14.0\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','101','101'),
('11','PC','LENOVO BRANDED','THINKCENTRE EDGE 92-1JA','CORE i3','WINDOWS 7 ENTERPRISE 64BIT',4,1000,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2012,'Expired','BELI',2013,'2','11','11'),
('12','PC','LENOVO BRANDED','THINKCENTRE EDGE 92-1JA','CORE i3','WINDOWS 7 ENTERPRISE 64BIT',4,1000,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2012,'Expired','BELI',2013,'2','12','12'),
('13','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','13','13'),
('14','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','14','14'),
('15','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS XP SP3 32BIT',2,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','15','15'),
('16','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','16','16'),
('17','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','17','17'),
('18','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','18','18'),
('19','PC','PC RAKITAN','RAKITAN','CORE i5','WINDOWS 7 ENTERPRISE 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2009,'Expired','BELI',2010,'2','19','19'),
('2','PC','PC RAKITAN','RAKITAN','CORE i5','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2009,'Expired','BELI',2010,'2','2','2'),
('20','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','20','20'),
('21','PC','LENOVO BRANDED','THINKCENTRE EDGE 92-1JA','CORE i3','WINDOWS 7 ENTERPRISE 64BIT',4,1000,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2012,'Expired','BELI',2013,'2','21','21'),
('22','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','22','22'),
('23','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','23','23'),
('24','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','24','24'),
('25','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','25','25'),
('26','PC','LENOVO BRANDED','THINKCENTRE EDGE 92-1JA','CORE i3','WINDOWS 7 ENTERPRISE 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2012,'Expired','BELI',2013,'2','26','26'),
('27','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','27','27'),
('28','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',2,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','28','28'),
('29','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',2,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','29','29'),
('3','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','3','3'),
('30','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS XP SP3 32BIT',2,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','30','30'),
('301','jenis','merk','type','prosesor','os',22,22,'vga','monitor','antar muka','mac',2010,'3 tahun','BELI',2011,'1','301','301'),
('31','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS XP SP3 32BIT',2,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','31','31'),
('32','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS XP SP3 32BIT',2,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','32','32'),
('33','PC','LENOVO BRANDED','LENOVO H50-50-B0ID','CORE i5','WINDOWS 7 ENTERPRISE 64BIT',4,1000,'NVIDIA ','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2015,'Expired','BELI',2015,'2','33','33'),
('34','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',6,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','34','34'),
('35','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','35','35'),
('36','PC','LENOVO BRANDED','THINKCENTRE EDGE 92-1JA','CORE i3','WINDOWS 7 ENTERPRISE 64BIT',8,1000,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2012,'Expired','BELI',2013,'2','36','36'),
('37','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','37','37'),
('38','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','38','38'),
('39','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','39','39'),
('4','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','4','4'),
('40','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','40','40'),
('41','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','41','41'),
('42','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','42','42'),
('43','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','43','43'),
('44','PC','LENOVO BRANDED','THINKCENTRE EDGE 92-1JA','CORE i3','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2012,'Expired','BELI',2013,'2','44','44'),
('45','PC','LENOVO BRANDED','THINKCENTRE EDGE 92-1JA','CORE i3','WINDOWS 7 ENTERPRISE 64BIT',4,1000,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2012,'Expired','BELI',2013,'2','45','45'),
('46','PC','LENOVO BRANDED','THINKCENTRE EDGE 92-1JA','CORE i3','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2012,'Expired','BELI',2013,'2','46','46'),
('47','PC','LENOVO BRANDED','THINKCENTRE EDGE 92-1JA','CORE i3','WINDOWS 7 ENTERPRISE 64BIT',4,1000,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2012,'Expired','BELI',2013,'2','47','47'),
('48','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','48','48'),
('49','PC','LENOVO BRANDED','THINKCENTRE EDGE 92-1JA','CORE i3','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2012,'Expired','BELI',2013,'2','49','49'),
('5','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','5','5'),
('50','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',2,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','50','50'),
('51','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','51','51'),
('52','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','52','52'),
('53','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','53','53'),
('54','PC','PC RAKITAN','RAKITAN','CORE i3','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2009,'Expired','BELI',2010,'2','54','54'),
('55','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','55','55'),
('56','PC','PC RAKITAN','RAKITAN','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2009,'Expired','BELI',2010,'2','56','56'),
('57','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','57','57'),
('58','PC','LENOVO BRANDED','THINKCENTRE EDGE 92-1JA','CORE i3','WINDOWS 7 ENTERPRISE 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2012,'Expired','BELI',2013,'2','58','58'),
('59','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','59','59'),
('6','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS XP SP3 32BIT',2,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','6','6'),
('60','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','60','60'),
('61','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','61','61'),
('62','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','62','62'),
('63','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','63','63'),
('64','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','64','64'),
('65','PC','LENOVO BRANDED','THINKCENTRE EDGE 92-1JA','CORE i3','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2012,'Expired','BELI',2013,'2','65','65'),
('66','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','66','66'),
('67','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','67','67'),
('68','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','68','68'),
('69','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','69','69'),
('7','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','7','7'),
('70','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','70','70'),
('71','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',2,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','71','71'),
('72','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',2,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','72','72'),
('73','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','73','73'),
('74','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','74','74'),
('75','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','75','75'),
('76','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS XP SP3 32BIT',2,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','76','76'),
('77','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','77','77'),
('78','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','78','78'),
('79','PC','LENOVO BRANDED','THINKCENTRE EDGE 92-1JA','CORE i3','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2012,'Expired','BELI',2013,'2','79','79'),
('8','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','8','8'),
('80','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','80','80'),
('81','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','81','81'),
('82','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','82','82'),
('83','PC','LENOVO BRANDED','THINKCENTRE EDGE 92-1JA','CORE i3','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2012,'Expired','BELI',2013,'2','83','83'),
('84','PC','LENOVO BRANDED','THINKCENTRE EDGE 92-1JA','CORE i3','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2012,'Expired','BELI',2013,'2','84','84'),
('85','PC','LENOVO BRANDED','THINKCENTRE EDGE 92-1JA','CORE i3','WINDOWS 7 PROFESSIONAL 32BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2012,'Expired','BELI',2013,'2','85','85'),
('86','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS XP SP3 32BIT',2,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','86','86'),
('87','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS XP SP3 32BIT',2,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','87','87'),
('88','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','88','88'),
('89','PC','PC RAKITAN','RAKITAN','CORE i3','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2009,'Expired','BELI',2010,'2','89','89'),
('9','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','9','9'),
('90','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS XP SP3 32BIT',2,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','90','90'),
('91','PC','PC RAKITAN','RAKITAN','CORE i3','WINDOWS 7 ENTERPRISE 64BIT',2,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2009,'Expired','BELI',2010,'2','91','91'),
('92','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ULTIMATE 32BIT',2,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','92','92'),
('93','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS XP SP3 32BIT',2,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','93','93'),
('94','LAPTOP','HP','PROBOOK 440 G3','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','14.0\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','94','94'),
('95','LAPTOP','HP','PROBOOK 440 G3','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','14.0\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','95','95'),
('96','LAPTOP','HP','PROBOOK 440 G3','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','14.0\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','96','96'),
('97','LAPTOP','HP','PROBOOK 440 G3','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','14.0\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','97','97'),
('98','LAPTOP','HP','PROBOOK 440 G3','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','14.0\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','98','98'),
('99','LAPTOP','HP','PROBOOK 440 G3','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','14.0\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','99','99');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` varchar(255) DEFAULT NULL,
  `username` varchar(30) DEFAULT NULL,
  `password` varchar(30) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `user` */

insert  into `user`(`id`,`username`,`password`,`token`,`nama`) values 
('1','admin','password',NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
