/*
SQLyog Ultimate v12.5.1 (32 bit)
MySQL - 10.4.11-MariaDB : Database - kmean_clustering
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`kmean_clustering` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `kmean_clustering`;

/*Table structure for table `bidang` */

DROP TABLE IF EXISTS `bidang`;

CREATE TABLE `bidang` (
  `id` varchar(255) NOT NULL,
  `nama_bidang` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `bidang` */

insert  into `bidang`(`id`,`nama_bidang`) values 
('1','JARINGAN'),
('10','TRANSAKSI ENERGI'),
('11','PEMASARAN'),
('2','PERENCANAAN'),
('3','FUNGSIONAL AHLI'),
('4','PENGADAAN'),
('5','KSA'),
('6','K2K3'),
('7','MANAGER'),
('8','KONSTRUKSI'),
('9','NIAGA');

/*Table structure for table `hibernate_sequence` */

DROP TABLE IF EXISTS `hibernate_sequence`;

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `hibernate_sequence` */

insert  into `hibernate_sequence`(`next_val`) values 
(1),
(1),
(1),
(1);

/*Table structure for table `kmean_setting` */

DROP TABLE IF EXISTS `kmean_setting`;

CREATE TABLE `kmean_setting` (
  `id` varchar(255) NOT NULL,
  `cluster` int(11) DEFAULT NULL,
  `treshold` double DEFAULT NULL,
  `variable` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `kmean_setting` */

insert  into `kmean_setting`(`id`,`cluster`,`treshold`,`variable`) values 
('1',3,0.8,'1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22');

/*Table structure for table `pemilik` */

DROP TABLE IF EXISTS `pemilik`;

CREATE TABLE `pemilik` (
  `id` varchar(255) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `pengelola` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `pemilik` */

insert  into `pemilik`(`id`,`nama`,`pengelola`) values 
('1','PT ICON+','UID BANTEN'),
('2','UP3 CIKOKOL','UP3 CIKOKOL');

/*Table structure for table `pengguna` */

DROP TABLE IF EXISTS `pengguna`;

CREATE TABLE `pengguna` (
  `id` varchar(255) NOT NULL,
  `nip` varchar(20) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `deskripsi` varchar(255) DEFAULT NULL,
  `bidang` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bidang` (`bidang`),
  CONSTRAINT `pengguna_ibfk_1` FOREIGN KEY (`bidang`) REFERENCES `bidang` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `pengguna` */

insert  into `pengguna`(`id`,`nip`,`nama`,`email`,`deskripsi`,`bidang`) values 
('1','','PETER SIREGAR','peter.siregar@pln.co.id','','1'),
('10','','SYAIFULLAH ARSYAD','syaifullah.arsyad@pln.co.id','','1'),
('100','','ANDI AULIA PUTRA','andi.aulia@pln.co.id','Manager Bidang 2','2'),
('101','','MUHAMMAD AZZAT KHAN','azzat.khan@pln.co.id','Staff Mapping','2'),
('11','','SASONGKO ARIBOWO','sasongko.aribowo@pln.co.id','','1'),
('12','','MAHADHIR','mahadhir@pln.co.id','','1'),
('13','','ASEP SUPRIYANTO','asep.supriyanto@pln.co.id','','1'),
('14','','DEDIN SAPARUDIN','dedin.saparudin@pln.co.id','','1'),
('15','','YUDHA EKO PRASETYO','yudha.prasetyo@pln.co.id','','1'),
('16','','ANDI AULIA PUTRA','andi.aulia@pln.co.id','','2'),
('17','','FUAD HASAN NAWAWI','fuad.hasan@pln.co.id','','2'),
('18','','DORA SEVMARMILA','dora.sevmarmila@pln.co.id','','2'),
('19','','KHAIRUL RIDHA','khairul.ridha@pln.co.id','','2'),
('2','','YOLANDA GUSTIAN','yolanda.gustian@pln.co.id','','1'),
('20','','ENJANG RAHMAN','enjang.rahman@pln.co.id','','2'),
('21','','FADIYA FIKROH NASYIROH','fadiya.fikroh@pln.co.id','','2'),
('22','','MUHAMMAD AZZAT KHAN','azzat.khan@pln.co.id','','2'),
('23','','FAUZIE','','Outsourcing','2'),
('24','','RIMA ANGGRAINI','','Outsourcing','2'),
('25','','INTAN MEILANI','','Outsourcing','2'),
('26','','JEPRI RAHMAT','','Outsourcing','2'),
('27','','MURDIN','murdin@pln.co.id','','3'),
('28','','ADING SURYADI','ading.suryadi@pln.co.id','','3'),
('29','','ANSORIZAL','ansorizal@pln.co.id','','3'),
('3','','MURTANI','murtani@pln.co.id','','1'),
('30','','HARTO','harto@pln.co.id','','3'),
('31','','YADI SUPRIYADI','yadi.supriyadi@pln.co.id','','3'),
('32','','YULIADI','yuliadi@pln.co.id','','3'),
('33','','HARI NADIRI SULISTIYONO','hari.sulistiyono@pln.co.id','','3'),
('34','','WILY SILVIYANTY','wily.silviyanty@pln.co.id','','4'),
('35','','HAERUL SALEH','haerul.saleh@pln.co.id','','4'),
('36','','RADEN RAMA PRAMUDYA KUSUMA','raden.kusuma@pln.co.id','','5'),
('37','','RIANI MASNAPITA SIHOMBING','riani.sihombing@pln.co.id','','5'),
('38','','MUAMMAR ARAFAT','muammar.arafat@pln.co.id','','5'),
('39','','MOHAMMAD ARIF FURHAN','moh.arif.furhan@pln.co.id','','5'),
('4','','ISA CANDI BIMO SAKTI','isa.sakti@pln.co.id','','1'),
('40','','KOKOM KOHAROCHMAH','kokom.koharochmah@pln.co.id','','5'),
('41','','SATRIA PRIBADI','satria.pribadi@pln.co.id','','5'),
('42','','AZIZAH KUSUMANINGRUM','azizah.kusumaningrum@pln.co.id','','5'),
('43','','ARYA BAGUS SANJAYA','arya.bagus@pln.co.id','','6'),
('44','','EDY MUKTASIM BILLAH','edy.muktasim@pln.co.id','','7'),
('45','','BAHNIARNO','bahniarno@pln.co.id','','8'),
('46','','ADI FITRIATMOJO','adi.fitriatmojo@pln.co.id','','8'),
('47','','NANANG DWI SUKMONO','nanang.ds@pln.co.id','','8'),
('48','','JUDI PAMUNGKAS','judi.pamungkas@pln.co.id','','8'),
('49','','ILHAM ABDILAH','ilham.abdilah@pln.co.id','','8'),
('5','','DESYTA AYU PANGESTIKA','destya.pangestika@pln.co.id','','1'),
('50','','PARIASTUTI','pariastuti@pln.co.id','','8'),
('51','','LUH AYU PRADYANI','ayu.pradyani@pln.co.id','','8'),
('52','','DWIEN CATRA CHRISANDY','catra.chrisandy@pln.co.id','','8'),
('53','','JAENUDIN','jaenudin@pln.co.id','','8'),
('54','','AGUS SUNARYO','agus.sunaryo@pln.co.id','','8'),
('55','','DIAN HARDIANA','dian.hardiana@pln.co.id','','8'),
('56','','ZAKIAH CUT MUTIA','','Outsourcing','8'),
('57','','NURLIANI','','Outsourcing','8'),
('58','','MUHAMMAD RIDUAN','muhammad.riduan@pln.co.id','','8'),
('59','','AGUS SULAEMAN','agus.sulaeman67m@pln.co.id','','8'),
('6','','CYBERLOCK','','Komputer Cyberlock Buka Akses Kunci Gardu','1'),
('60','','HERU EKO PRAYITNO','heru.prayitno@pln.co.id','','9'),
('61','','RATNA PERTIWI','ratna.pertiwi@pln.co.id','','9'),
('62','','ERLIN WULAN SARI','erlin.wulan.sari@pln.co.id','','9'),
('63','','ARYANDRA ANDARU','aryandra.andaru@pln.co.id','','9'),
('64','','HERDI RISKY PARTIARTANTO','herdi.risky@pln.co.id','','9'),
('65','','VINCENTIUS AGUNG PRIBADI','vincentius.AP@pln.co.id','','9'),
('66','','RIZKI NUR AULIA','nur.aulia@pln.co.id','','9'),
('67','','BIANDA TALITHA UTARI AYU','bianda.ayu@pln.co.id','','9'),
('68','','KETY HELKI BIANG','kety.helki@pln.co.id','','9'),
('69','','ARIS MUNANDAR','aris.munandar@pln.co.id','','9'),
('7','','HASIAN EFRAVODITUS SITORUS','hasian.efravoditus@pln.co.id','','1'),
('70','','AZIZAH','azizah@pln.co.id','','9'),
('71','','FRONT LINEAR 1','','Outsourcing','9'),
('72','','FRONT LINEAR 2','','Outsourcing','9'),
('73','','HERI DWI SULISTYO','heri.dwi@pln.co.id ','','10'),
('74','','M. GUNAWAN SATHORI MANGULUANG','gunawan.sathori@pln.co.id','','10'),
('75','','IRWANIDA','irwanida@pln.co.id','','10'),
('76','','KUKUH RADIYANTO','kukuh.radiyanto@pln.co.id','','10'),
('77','','ADILLA SYAMSA','adilla.syamsa@pln.co.id','','10'),
('78','','PIAN SUHARDIANSYAH','pian.suhardiansyah@pln.co.id','','10'),
('79','','FADLI ARISTIYAN','fadli.aristiyan@pln.co.id','','10'),
('8','','YUDHA DWI PAMBUDI','yudha.dwi.pambudi@pln.co.id','','1'),
('80','','RINANTO DEMI NUGROHO','rinanto.nugroho@pln.co.id','','10'),
('81','','NI NYOMAN WIRATI','ni.nyoman.wirati@pln.co.id','','10'),
('82','','TEGUH HAQIQI','teguh.haqiqi@pln.co.id','','10'),
('83','','ANGGA PRADANA','angga.pradana@pln.co.id','','10'),
('84','','JUWITA RESTIANISA','juwita.restianisa@pln.co.id','','10'),
('85','','AMR1','','Outsourcing','10'),
('86','','AMR2','','Outsourcing','10'),
('87','','AMR3','','Outsourcing','10'),
('88','','CATER1','','Outsourcing','10'),
('89','','CATER2','','Outsourcing','10'),
('9','','FAIZAL FIKRY','faizal.fikry@pln.co.id','','1'),
('90','','CATER3','','Outsourcing','10'),
('91','','P2TL1','','Outsourcing','10'),
('92','','P2TL2','','Outsourcing','10'),
('93','','P2TL3','','Outsourcing','10'),
('94','','SASONGKO ARIBOWO','sasongko.aribowo@pln.co.id','Manager Bidang 1','1'),
('95','','NANANG DWI SUKMONO','nanang.ds@pln.co.id','Manager Bidang 8','8'),
('96','','M. GUNAWAN SATHORI MANGULUANG','gunawan.sathori@pln.co.id','Manager Bidang 10','10'),
('97','','HERU EKO PRAYITNO','heru.prayitno@pln.co.id','Manager Bidang 9','9'),
('98','','AZIZAH','azizah@pln.co.id','Manager Bidang 11','11'),
('99','','AZIZAH KUSUMANINGRUM','azizah.kusumaningrum@pln.co.id','Manager Bidang 5','5');

/*Table structure for table `perangkat` */

DROP TABLE IF EXISTS `perangkat`;

CREATE TABLE `perangkat` (
  `id` varchar(255) NOT NULL,
  `jenis` varchar(255) DEFAULT NULL,
  `merk` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `prosesor` varchar(255) DEFAULT NULL,
  `os` varchar(255) DEFAULT NULL,
  `ram` int(11) DEFAULT NULL,
  `hdd` int(11) DEFAULT NULL,
  `vga` varchar(255) DEFAULT NULL,
  `monitor` varchar(255) DEFAULT NULL,
  `antarmuka` varchar(255) DEFAULT NULL,
  `mac_address` varchar(255) DEFAULT NULL,
  `tahun_pembuatan` int(4) DEFAULT NULL,
  `garansi` varchar(255) DEFAULT NULL,
  `status_perangkat` varchar(20) DEFAULT NULL,
  `tahun_awal_operasi` int(4) DEFAULT NULL,
  `id_owner` varchar(255) DEFAULT NULL,
  `id_pengguna` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_owner` (`id_owner`),
  KEY `id_pengguna` (`id_pengguna`),
  CONSTRAINT `perangkat_ibfk_1` FOREIGN KEY (`id_owner`) REFERENCES `pemilik` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `perangkat_ibfk_2` FOREIGN KEY (`id_pengguna`) REFERENCES `pengguna` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `perangkat` */

insert  into `perangkat`(`id`,`jenis`,`merk`,`type`,`prosesor`,`os`,`ram`,`hdd`,`vga`,`monitor`,`antarmuka`,`mac_address`,`tahun_pembuatan`,`garansi`,`status_perangkat`,`tahun_awal_operasi`,`id_owner`,`id_pengguna`) values 
('1','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','1'),
('10','PC','LENOVO BRANDED','THINKCENTRE EDGE 92-1JA','CORE i3','WINDOWS 7 ENTERPRISE 64BIT',8,1000,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2012,'Expired','BELI',2013,'2','10'),
('100','LAPTOP','HP','PROBOOK 440 G3','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','14.0\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','100'),
('101','LAPTOP','HP','PROBOOK 440 G3','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','14.0\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','101'),
('11','PC','LENOVO BRANDED','THINKCENTRE EDGE 92-1JA','CORE i3','WINDOWS 7 ENTERPRISE 64BIT',4,1000,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2012,'Expired','BELI',2013,'2','11'),
('12','PC','LENOVO BRANDED','THINKCENTRE EDGE 92-1JA','CORE i3','WINDOWS 7 ENTERPRISE 64BIT',4,1000,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2012,'Expired','BELI',2013,'2','12'),
('13','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','13'),
('14','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','14'),
('15','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS XP SP3 32BIT',2,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','15'),
('16','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','16'),
('17','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','17'),
('18','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','18'),
('19','PC','PC RAKITAN','RAKITAN','CORE i5','WINDOWS 7 ENTERPRISE 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2009,'Expired','BELI',2010,'2','19'),
('2','PC','PC RAKITAN','RAKITAN','CORE i5','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2009,'Expired','BELI',2010,'2','2'),
('20','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','20'),
('21','PC','LENOVO BRANDED','THINKCENTRE EDGE 92-1JA','CORE i3','WINDOWS 7 ENTERPRISE 64BIT',4,1000,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2012,'Expired','BELI',2013,'2','21'),
('22','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','22'),
('23','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','23'),
('24','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','24'),
('25','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','25'),
('26','PC','LENOVO BRANDED','THINKCENTRE EDGE 92-1JA','CORE i3','WINDOWS 7 ENTERPRISE 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2012,'Expired','BELI',2013,'2','26'),
('27','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','27'),
('28','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',2,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','28'),
('29','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',2,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','29'),
('3','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','3'),
('30','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS XP SP3 32BIT',2,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','30'),
('31','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS XP SP3 32BIT',2,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','31'),
('32','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS XP SP3 32BIT',2,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','32'),
('33','PC','LENOVO BRANDED','LENOVO H50-50-B0ID','CORE i5','WINDOWS 7 ENTERPRISE 64BIT',4,1000,'NVIDIA ','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2015,'Expired','BELI',2015,'2','33'),
('34','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',6,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','34'),
('35','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','35'),
('36','PC','LENOVO BRANDED','THINKCENTRE EDGE 92-1JA','CORE i3','WINDOWS 7 ENTERPRISE 64BIT',8,1000,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2012,'Expired','BELI',2013,'2','36'),
('37','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','37'),
('38','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','38'),
('39','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','39'),
('4','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','4'),
('40','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','40'),
('41','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','41'),
('42','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','42'),
('43','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','43'),
('44','PC','LENOVO BRANDED','THINKCENTRE EDGE 92-1JA','CORE i3','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2012,'Expired','BELI',2013,'2','44'),
('45','PC','LENOVO BRANDED','THINKCENTRE EDGE 92-1JA','CORE i3','WINDOWS 7 ENTERPRISE 64BIT',4,1000,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2012,'Expired','BELI',2013,'2','45'),
('46','PC','LENOVO BRANDED','THINKCENTRE EDGE 92-1JA','CORE i3','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2012,'Expired','BELI',2013,'2','46'),
('47','PC','LENOVO BRANDED','THINKCENTRE EDGE 92-1JA','CORE i3','WINDOWS 7 ENTERPRISE 64BIT',4,1000,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2012,'Expired','BELI',2013,'2','47'),
('48','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','48'),
('49','PC','LENOVO BRANDED','THINKCENTRE EDGE 92-1JA','CORE i3','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2012,'Expired','BELI',2013,'2','49'),
('5','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','5'),
('50','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',2,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','50'),
('51','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','51'),
('52','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','52'),
('53','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','53'),
('54','PC','PC RAKITAN','RAKITAN','CORE i3','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2009,'Expired','BELI',2010,'2','54'),
('55','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','55'),
('56','PC','PC RAKITAN','RAKITAN','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2009,'Expired','BELI',2010,'2','56'),
('57','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','57'),
('58','PC','LENOVO BRANDED','THINKCENTRE EDGE 92-1JA','CORE i3','WINDOWS 7 ENTERPRISE 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2012,'Expired','BELI',2013,'2','58'),
('59','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','59'),
('6','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS XP SP3 32BIT',2,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','6'),
('60','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','60'),
('61','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','61'),
('62','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','62'),
('63','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','63'),
('64','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','64'),
('65','PC','LENOVO BRANDED','THINKCENTRE EDGE 92-1JA','CORE i3','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2012,'Expired','BELI',2013,'2','65'),
('66','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','66'),
('67','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','67'),
('68','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','68'),
('69','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','69'),
('7','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','7'),
('70','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','70'),
('71','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',2,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','71'),
('72','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',2,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','72'),
('73','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','73'),
('74','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','74'),
('75','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','75'),
('76','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS XP SP3 32BIT',2,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','76'),
('77','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','77'),
('78','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','78'),
('79','PC','LENOVO BRANDED','THINKCENTRE EDGE 92-1JA','CORE i3','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2012,'Expired','BELI',2013,'2','79'),
('8','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','8'),
('80','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','80'),
('81','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','81'),
('82','PC','HP BRANDED','HP 280 G3 MT BUSINESS','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','82'),
('83','PC','LENOVO BRANDED','THINKCENTRE EDGE 92-1JA','CORE i3','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2012,'Expired','BELI',2013,'2','83'),
('84','PC','LENOVO BRANDED','THINKCENTRE EDGE 92-1JA','CORE i3','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2012,'Expired','BELI',2013,'2','84'),
('85','PC','LENOVO BRANDED','THINKCENTRE EDGE 92-1JA','CORE i3','WINDOWS 7 PROFESSIONAL 32BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2012,'Expired','BELI',2013,'2','85'),
('86','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS XP SP3 32BIT',2,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','86'),
('87','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS XP SP3 32BIT',2,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','87'),
('88','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','88'),
('89','PC','PC RAKITAN','RAKITAN','CORE i3','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2009,'Expired','BELI',2010,'2','89'),
('9','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ENTERPRISE 64BIT',4,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','9'),
('90','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS XP SP3 32BIT',2,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','90'),
('91','PC','PC RAKITAN','RAKITAN','CORE i3','WINDOWS 7 ENTERPRISE 64BIT',2,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2009,'Expired','BELI',2010,'2','91'),
('92','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS 7 ULTIMATE 32BIT',2,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','92'),
('93','PC','LENOVO BRANDED','THINKCENTRE EDGE 71-H8A','DUAL CORE','WINDOWS XP SP3 32BIT',2,500,'Integrated','18.5\" (Non Touch Screen)','USB / HDMI / VGA','',2010,'Expired','BELI',2012,'2','93'),
('94','LAPTOP','HP','PROBOOK 440 G3','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','14.0\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','94'),
('95','LAPTOP','HP','PROBOOK 440 G3','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','14.0\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','95'),
('96','LAPTOP','HP','PROBOOK 440 G3','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','14.0\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','96'),
('97','LAPTOP','HP','PROBOOK 440 G3','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','14.0\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','97'),
('98','LAPTOP','HP','PROBOOK 440 G3','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','14.0\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','98'),
('99','LAPTOP','HP','PROBOOK 440 G3','CORE i5','WINDOWS 10 PRO N 64BIT',8,500,'Integrated','14.0\" (Non Touch Screen)','USB / HDMI / VGA','',2018,'3 Tahun','SEWA',2019,'1','99');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` varchar(255) DEFAULT NULL,
  `username` varchar(30) DEFAULT NULL,
  `password` varchar(30) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `user` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
